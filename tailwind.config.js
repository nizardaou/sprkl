module.exports = {
  purge: ["./pages/**/*.{js,ts,jsx,tsx}", "./components/**/*.{js,ts,jsx,tsx}"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      backgroundColor: (theme) => ({
        ...theme("colors"),
        primary: "#1C1D1F",
        secondary: "#00FFE6",
        dropdown: "#1E1E1E",
        box: "#2D2F31",
        box1: "#47BDAC",
        box2: "#383C46",
        box3: "#6A6C7D",
        box4: "#2A9AD3",
      }),
      textColor: {
        white: "#fff",
        cyan: "#00FFE6",
        gray: "#656565",
        gray2: "#EBEDED",
        box: "#2A2A2A",
        purple: "#FF00AA",
      },
      fontFamily: {
        merriweather: ["Merriweather"],
        futuramaxi: ["Futura Maxi CG"],
      },
      fontSize: {
        "custom-3xl": ["30px", "43px"],
      },
      borderColor: {
        cyan: "#00FFE6",
        box: "#2D2F31",
      },
      minWidth: {
        14: "3.5rem",
      },
      minHeight: {
        14: "3.5rem",
        56: "14rem",
      },
      height: {
        400: "400px",
        450: "450px",
        500: "500px",
        600: "600px",
        700: "700px",
        800: "800px",
      },
      backgroundPosition: {
        hero: "50% 80%",
        "hero-mobile": "25% 70%",
        "hero-bottom": "10% 60%",
      },
      colors: {
        custom: {
          cyan: "#00FFE6",
        },
      },
    },
  },
  variants: {
    extend: {},
  },
};
