import Link from "next/link";
import AnimatedLink from "./animatedlink";

export default function Footer() {
  const footerNavigation = [
    { name: "grow@sprkl.co", href: "mailto:grow@sprkl.co" },
    { name: "+971.52.244.6929", href: "tel:+971522446929" },
  ];

  return (
    <div className="bg-primary relative">
      <div className="lg:max-w-7xl w-5/6 mx-auto flex flex-col lg:flex-row justify-start items-start lg:items-center text-white py-20 lg:py-24 font-futuramaxi font-normal">
        {footerNavigation.map((item, index) => (
          <div
            key={index}
            className={
              index == 0
                ? "flex flex-auto lg:mr-5 lg:w-40 xl:w-24"
                : " lg:justify-start flex flex-auto lg:w-96"
            }
          >
            <AnimatedLink
              extraClasses="w-52"
              extraContainerClass="lg:max-w-7xl w-5/6"
              href={item.href}
            >
              {item.name}
            </AnimatedLink>
          </div>
        ))}
        <div className="flex-auto flex flex-col justify-center mt-10 lg:mt-0">
          <p className="self-start lg:self-end">
            DTEC. Dubai Silicon Oasis. UAE
          </p>
          <small className="font-light lg:pl-16 self-start lg:self-end">
            © 2021 sprkl - fzco.{" "}
            <Link href="/privacy" scroll={false}>
              <a className="underline text-white hover:text-cyan">
                Tems & Privacy
              </a>
            </Link>
          </small>
        </div>
      </div>
    </div>
  );
}
