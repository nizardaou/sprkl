import { useEffect, useRef, useState } from "react";
import PopOver from "./popover";
import Toggle from "../components/svg/toggle";
import Remove from "../components/svg/remove";
import onClickOutside from "react-onclickoutside";
import gsap from "gsap/dist/gsap";
import { useRouter } from "next/router";
import { ScrollToPlugin } from "gsap/dist/ScrollToPlugin";
gsap.registerPlugin(ScrollToPlugin);

function MultiSelect(props) {
  let router = useRouter();

  const { droppedItems, type, chosenItems, name } = props;
  // contains selected items
  const [selectedItems, setSelected] = useState([]);
  const [dropDown, setDropdown] = useState(false);
  let toggle = useRef(null);

  MultiSelect["handleClickOutside_" + name] = () => setDropdown(false);

  const toggleDropdown = () => {
    setDropdown(!dropDown);
  };

  // adds new item to multiselect
  const addTag = (item) => {
    if (!selectedItems.includes(item)) {
      setSelected((prevItems) => [...prevItems, item]);
    }
  };
  // removes item from multiselect
  const removeTag = (item) => {
    setDropdown(false);
    const filtered = selectedItems.filter((e) => e !== item);
    setSelected(filtered);
  };

  useEffect(() => {
    if (name === "focus") {
      if (router.asPath.includes("healthcare")) {
        setSelected(["healthcare"]);
      } else if (router.asPath.includes("egaming")) {
        setSelected(["egaming"]);
      } else if (router.asPath.includes("public&ngo")) {
        setSelected(["public & ngo"]);
      } else if (router.asPath.includes("f&b")) {
        setSelected(["f&b"]);
      }
    }

    if (name === "competence") {
      if (router.asPath.includes("strategy")) {
        setSelected(["strategy"]);
      } else if (router.asPath.includes("brand")) {
        setSelected(["brand"]);
      } else if (router.asPath.includes("content")) {
        setSelected(["content"]);
      } else if (router.asPath.includes("tech")) {
        setSelected(["tech"]);
      } else if (router.asPath.includes("media")) {
        setSelected(["media"]);
      } else if (router.asPath.includes("insights")) {
        setSelected(["insights"]);
      }
    }
  }, []);

  useEffect(() => {
    chosenItems(selectedItems);
    if (dropDown) {
      gsap.to(toggle.current, { rotation: "180_cw", duration: 0.5 });
    } else if (!dropDown) {
      gsap.to(toggle.current, { rotation: "0_cw", duration: 0.5 });
    }
  }, [dropDown, selectedItems]);

  return (
    <div onClick={toggleDropdown} className="flex flex-col items-center">
      <div className="w-full relative">
        <div className="flex flex-col items-center relative">
          <div className="w-full ">
            <div className="my-2 flex border-box rounded-lg border-4 ">
              <div className="flex flex-auto flex-wrap min-h-14 h-auto">
                {selectedItems.map((tag, index) => {
                  return (
                    <div
                      onClick={(e) => e.stopPropagation()}
                      key={index}
                      className="flex justify-center border-2 border-cyan rounded-lg items-center m-1 font-medium py-1 px-2 bg-primary text-white "
                    >
                      <div className="text-lg flex items-center p-1 font-futuramaxi font-normal leading-none max-w-full flex-initial">
                        {tag}
                      </div>
                      <div className="flex items-center cursor-pointer ml-3 flex-auto flex-row-reverse">
                        <div onClick={() => removeTag(tag)}>
                          <Remove />
                        </div>
                      </div>
                    </div>
                  );
                })}
                {selectedItems.length == 0 && (
                  <div className="flex-1 bg-primary">
                    <input
                      disabled
                      placeholder={"select a" + " " + type}
                      className="bg-transparent h-14 placeholder-white font-futuramaxi text-lg sm:text-xl p-1 pl-5 pr-2 appearance-none outline-none w-full text-gray-800"
                    />
                  </div>
                )}
              </div>
              <div
                className="bg-box border-box w-14 min-w-14 min-h-full flex p-2 border-l"
                onClick={toggleDropdown}
              >
                <button className="cursor-pointer w-full h-full outline-none focus:outline-none">
                  <Toggle ref={toggle} />
                </button>
              </div>
            </div>
          </div>
        </div>
        {dropDown && (
          <PopOver
            list={droppedItems}
            selectedItems={selectedItems}
            addItem={addTag}
          />
        )}
      </div>
    </div>
  );
}
const clickOutsideConfig = {
  handleClickOutside: ({ props }) =>
    MultiSelect["handleClickOutside_" + props.name],
};

export default onClickOutside(MultiSelect, clickOutsideConfig);
