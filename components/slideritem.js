import { useRef, useEffect, useState } from "react";
import { gsap } from "gsap/dist/gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import { useRouter } from "next/router";
gsap.registerPlugin(ScrollTrigger);

export default function SliderItem(props) {
  const {
    project,
    level,
    carouselLeft,
    carouselRight,
    carouselContent,
    firstRenderTop,
  } = props;
  let router = useRouter();
  let heading = useRef();
  let item = useRef();
  const [firstRender, setFirstRender] = useState(true);
  // console.log("firstRenderTop", firstRenderTop);

  function handleClick(link) {
    if (level === -1) {
      carouselRight();
      setFirstRender(false);
    } else if (level === 1) {
      carouselLeft();
      setFirstRender(false);
    } else router.push(link, undefined, { scroll: false });
  }
  useEffect(() => {
    // console.log(project.linkHref);
    if (level === 0 && (!firstRender || !firstRenderTop)) {
      carouselContent(
        project.link,
        project.linkHref,
        project.subtitle,
        project.title
      );
    } else if (level === 0 && firstRender && firstRenderTop) {
      carouselContent(project.link, project.linkHref, project.subtitle);
    }
  }, [level]);

  return (
    <>
      <h1
        onClick={() => handleClick(project.href)}
        ref={heading}
        className={
          `font-futuramaxi absolute cursor-pointer slider-item font-normal text-4xl md:text-6xl xl:text-7xl  ${
            level == 0 ? "text-cyan" : "inline-block text-white"
          }` + `${" " + "slider-level" + level}`
        }
      >
        {project.title}
      </h1>
    </>
  );
}
