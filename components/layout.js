import { useState, useEffect } from "react";
import styles from "../styles/layout.module.css";
import Footer from "./footer";
import Header from "./header";
import FontFaceObserver from "fontfaceobserver";
import ProgressBar from "@badrap/bar-of-progress";
import { useRouter } from "next/router";
import Head from "next/head";

const progress = new ProgressBar({
  size: 5,
  color: "#00FFE6",
  delay: 0,
  className: "preloader",
});

export default function TransitionLayout({ children }) {
  let router = useRouter();
  var merriWeather = new FontFaceObserver("Merriweather");
  var futuraMaxi = new FontFaceObserver("Futura Maxi CG");

  const [displayChildren, setDisplayChildren] = useState(children);
  const [transitionStage, setTransitionStage] = useState("fadeOut");
  const [loadedFonts, setLoadedFonts] = useState(false);
  const [loadedImages, setLoadedImages] = useState(false);
  // const [loadedFirst, setLoadedFirst] = useState(false);
  // const [loadedSecond, setLoadedSecond] = useState(false);
  // const [loadedThird, setLoadedThird] = useState(false);
  // const [loadedFourth, setLoadedFourth] = useState(false);

  useEffect(() => {
    // start progress bar animation
    progress.start();

    //video preloader
    // function onSuccess(url) {
    //   if (url.includes("eGaming")) {
    //     console.log("eGaming loaded");
    //     setLoadedFirst(true);
    //   } else if (url.includes("FNB")) {
    //     console.log("FNB loaded");
    //     setLoadedSecond(true);
    //   } else if (url.includes("Healthcare")) {
    //     console.log("Healthcare loaded");
    //     setLoadedThird(true);
    //   } else if (url.includes("Ngo")) {
    //     console.log("Ngo loaded");
    //     setLoadedFourth(true);
    //   }
    // }

    // function onError() {
    //   console.log("there was an error");
    // }
    // // calling video preloader function
    // prefetch_file("/videos/eGaming.webm", onSuccess, onError);
    // prefetch_file("/videos/FNB.webm", onSuccess, onError);
    // prefetch_file("/videos/Healthcare.webm", onSuccess, onError);
    // prefetch_file("/videos/Ngo.webm", onSuccess, onError);

    // // video preloader function which creates an xhr request that return a blob object as a response
    // function prefetch_file(url, fetched_callback, error_callback) {
    //   var xhr = new XMLHttpRequest();
    //   xhr.open("GET", url, true);
    //   xhr.responseType = "blob";

    //   xhr.addEventListener(
    //     "load",
    //     function () {
    //       if (xhr.status === 200) {
    //         var URL = window.URL || window.webkitURL;
    //         var blob_url = URL.createObjectURL(xhr.response);
    //         fetched_callback(url);
    //       } else {
    //         error_callback();
    //       }
    //     },
    //     false
    //   );

    //   xhr.send();
    // }

    // preload image function
    function preloadImages(array) {
      const loadPromises = array.map((image) => {
        return new Promise((resolve, reject) => {
          const img = new Image();
          img.src = image;
          img.onload = () => resolve(true);
        });
      });

      Promise.all(loadPromises)
        .then(() => {
          setLoadedImages(true);
        })
        .catch((err) => console.log(err));
    }
    // preloaded images based on routes
    if (router.asPath === "/") {
      preloadImages([
        "/images/hero/L0.jpg",
        "/images/hero/L1.png",
        "/images/hero/L2.png",
        "/images/hero/L3.png",
        "/images/hero/L4.png",
        "/images/hero/L5.png",
        "/images/hero/L6.png",
        "/images/hero/L7.png",
        "/images/Competence/Brand.jpg",
        "/images/Competence/Content.jpg",
        "/images/Competence/Strategy.jpg",
        "/images/Competence/Tech.jpg",
        "/images/Competence/Media.jpg",
        "/images/Competence/Insights.jpg",
      ]);
    } else if (router.asPath.includes("about")) {
      preloadImages([
        "/images/about/1.png",
        "/images/about/2.png",
        "/images/about/3.png",
        "/images/about/4.png",
        "/images/about/5.png",
        "/images/about/6.png",
        "/images/about/7.png",
        "/images/about/8.png",
      ]);
    } else if (router.asPath.includes("focus")) {
      preloadImages([
        "/images/Banners/Focus/FNB.jpg",
        "/images/Banners/Focus/gaming.jpg",
        "/images/Banners/Focus/public&ngo.jpg",
        "/images/Banners/Focus/Healthcare.jpg",
      ]);
    } else if (router.asPath.includes("echir")) {
      preloadImages(["/images/Banners/E-CHIR.jpg"]);
    } else if (router.asPath.includes("afriplay")) {
      preloadImages(["/images/Banners/AFRIPLAY.jpg"]);
    } else if (router.asPath.includes("yalla")) {
      preloadImages(["/images/Banners/YALLA-SHAWARMA.jpg"]);
    } else if (router.asPath.includes("omac")) {
      preloadImages(["/images/Banners/OMAC.jpg"]);
    } else if (router.asPath.includes("usaid")) {
      preloadImages(["/images/Banners/USAID.jpg"]);
    } else if (router.asPath.includes("diego")) {
      preloadImages(["/images/Banners/Diego-Burgers.jpg"]);
    } else if (router.asPath.includes("nabad")) {
      preloadImages(["/images/Banners/NABAD.jpg"]);
    } else if (router.asPath.includes("eu")) {
      preloadImages(["/images/Banners/EU.jpg"]);
    } else setLoadedImages(true);

    // fonts preloader
    Promise.all([
      merriWeather.load(null, 60000),
      futuraMaxi.load(null, 60000),
    ]).then(function () {
      setLoadedFonts(true);
    });

    // fade in page animation
    setTransitionStage("fadeIn");
  }, [router.asPath]);

  useEffect(() => {
    if (loadedImages && loadedFonts) {
      // finish progress bar animation
      setTimeout(() => {
        progress.finish();
      }, 1000);
    }
  }, [loadedImages, loadedFonts, router.asPath]);

  useEffect(() => {
    if (children !== displayChildren) {
      // console.log(displayChildren.type.name);
      setTransitionStage("fadeOut");
    }
  }, [children, displayChildren]);

  return (
    <>
      <style jsx global>{`
        /*---Merriweather font import--- */

        @font-face {
          font-family: "Merriweather";
          src: url("/fonts/Merriweather-Light.ttf") format("truetype");
          font-weight: 300;
          font-display: block;
        }

        @font-face {
          font-family: "Merriweather";
          src: url("/fonts/Merriweather-Bold.ttf") format("truetype");
          font-weight: 700;
          font-display: block;
        }

        @font-face {
          font-family: "Merriweather";
          src: url("/fonts/Merriweather-UltraBdIt.ttf") format("truetype");
          font-weight: 900;
          font-style: italic;
          font-display: block;
        }

        @font-face {
          font-family: "Merriweather";
          src: url("/fonts/Merriweather-UltraBold.ttf") format("truetype");
          font-weight: 900;
          font-display: block;
        }

        @font-face {
          font-family: "Merriweather";
          src: url("/fonts/Merriweather-BoldIt.ttf") format("truetype");
          font-weight: 700;
          font-style: italic;
          font-display: block;
        }

        @font-face {
          font-family: "Merriweather";
          src: url("/fonts/Merriweather-Italic.ttf") format("truetype");
          font-weight: normal;
          font-style: italic;
          font-display: block;
        }

        @font-face {
          font-family: "Merriweather";
          src: url("/fonts/Merriweather-LightIt.ttf") format("truetype");
          font-weight: 300;
          font-style: italic;
          font-display: block;
        }

        @font-face {
          font-family: "Merriweather";
          src: url("/fonts/Merriweather-Regular.ttf") format("truetype");
          font-weight: 400;
          font-display: block;
        }

        /*---Futura Maxi CG font import--- */

        @font-face {
          font-family: "Futura Maxi CG";
          src: url("/fonts/Futura-Maxi-CG-Bold-Regular.otf") format("opentype");
          font-weight: 400;
          font-display: block;
        }

        @font-face {
          font-family: "Futura Maxi CG";
          src: url("/fonts/Futura-Maxi-CG-Book-Regular.otf") format("opentype");
          font-weight: 900;
          font-display: block;
        }

        @font-face {
          font-family: "Futura Maxi CG";
          src: url("/fonts/Futura-Maxi-CG-Demi-Regular.otf") format("opentype");
          font-weight: 700;
          font-display: block;
        }

        @font-face {
          font-family: "Futura Maxi CG";
          src: url("/fonts/Futura-Maxi-CG-Light-Regular.otf") format("opentype");
          font-weight: 300;
          font-display: block;
        }
      `}</style>
      <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta charSet="utf-8" />
      </Head>

      {loadedImages && loadedFonts && (
        <div>
          <Header />
          <div
            onTransitionEnd={() => {
              if (transitionStage === "fadeOut") {
                window.scrollTo(0, 0);
                setDisplayChildren(children);
                setTransitionStage("fadeIn");
              }
            }}
            className={`${styles.content} ${styles[transitionStage]}`}
          >
            {displayChildren}
          </div>
          <Footer />
        </div>
      )}
    </>
  );
}
