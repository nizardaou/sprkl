export default function Box({ bgColor, alignItems, children }) {
  return (
    <div
      className={alignItems + " " + bgColor + " " + "pl-4 flex h-72 bg-cover"}
    >
      <p className="text-white font-merriweather font-normal my-5 text-2xl">
        {children}
      </p>
    </div>
  );
}
