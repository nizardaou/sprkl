import Link from "next/link";
import { gsap } from "gsap/dist/gsap";
import { useRef, useEffect, useState } from "react";
import styles from "../styles/link.module.css";

export default function AnimatedLink({ children, ...props }) {
  const { href, extraClasses, extraContainerClass, type, blank } = props;
  let link = useRef(null);
  const [linkAnimation, setLinkAnimation] = useState();
  useEffect(() => {
    setLinkAnimation(
      gsap
        .fromTo(
          link.current,
          { color: "#fff", backgroundPosition: "200%", backgroundSize: "300%" },
          {
            backgroundImage:
              "linear-gradient(to left,rgba(0,255,230,1) 46%, rgba(255,0,170,1) 56%)",
            backgroundPosition: "100%",
            color: "transparent",
            backgroundSize: "300%",
            duration: 0.5,
          }
        )
        .pause()
    );
  }, []);

  function animateLink(status) {
    if (status === "mouseEnter" && linkAnimation) {
      linkAnimation.restart();
    } else if (status === "mouseLeave" && linkAnimation) {
      linkAnimation.reverse();
    }
  }

  return (
    <div className={type === "menu" ? "inline-block" : extraContainerClass}>
      <Link href={href} scroll={false}>
        <a
          ref={link}
          target={blank ? "_blank" : null}
          onMouseOver={() => {
            if (window.innerWidth > 768) animateLink("mouseEnter");
          }}
          onMouseLeave={() => {
            if (window.innerWidth > 768) animateLink("mouseLeave");
          }}
          className={
            type === "menu"
              ? extraClasses + " " + "bg-clip-text py-1 pointer-events-auto"
              : type === "carousel"
              ? extraClasses + " " + "pointer-events-auto bg-clip-text"
              : "self-center my-1 flex flex-auto justify-self-start items-center overflow-hidden cursor-pointer bg-clip-text text-white font-futuramaxi font-normal pointer-events-auto" +
                " " +
                extraClasses +
                " " +
                styles.animated
          }
        >
          {children}
        </a>
      </Link>
    </div>
  );
}
