import { useEffect, useRef, useState } from "react";
import AnimatedLink from "./animatedlink";
import Email from "./svg/email";
import Whatsapp from "./svg/whatsapp";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import { gsap, Power2, Bounce } from "gsap/dist/gsap";

gsap.registerPlugin(ScrollTrigger);

export default function BoxContact(props) {
  const { item } = props;
  const [animation, setAnimation] = useState();
  const [status, setStatus] = useState("");

  let box = useRef();
  useEffect(() => {
    if (window.innerWidth < 767) {
      gsap.to(box.current, {
        borderColor: "#00FFE6",
        backgroundColor: "#1C1D1F",
        duration: 0.5,
        scrollTrigger: {
          id: "mobileBox",
          trigger: box.current,
          start: "top center",
          end: "bottom center",
          toggleActions: "play reverse play reverse",
        },
      });
    }

    setAnimation(
      gsap
        .to(box.current, {
          borderColor: "#00FFE6",
          backgroundColor: "#1C1D1F",
          duration: 0.5,
        })
        .pause()
    );
  }, []);
  function animateMouseOver() {
    animation.play();
    setStatus("mouseEnter");
  }

  function animateMouseLeave() {
    animation.reverse();
    setStatus("mouseLeave");
  }
  return (
    <div
      ref={box}
      onMouseOver={animateMouseOver}
      onMouseLeave={animateMouseLeave}
      className={
        "bg-box border-transparent border-4 pl-10 pr-5 lg:px-10  flex flex-col h-96 pt-10 bg-cover"
      }
    >
      <h2 className="text-7xl lg:text-7xl font-futuramaxi font-normal text-cyan">
        {item.domain}
      </h2>
      <p className="text-white font-merriweather font-normal mt-5 text-xl">
        {item.firstAddress}
      </p>
      <p className="text-white font-merriweather font-normal mb-5 text-xl">
        {item.secondAddress}
      </p>
      <div className="">
        <a href={"mailto:" + item.email}>
          <Email />
        </a>
        <a href={"https://wa.me/" + item.whatsapp}>
          <Whatsapp />
        </a>
      </div>
      <AnimatedLink
        parentStatus={status}
        href={"tel:" + item.phone}
        extraClasses="mt-10 text-xl"
      >
        {item.phone}
      </AnimatedLink>
    </div>
  );
}
