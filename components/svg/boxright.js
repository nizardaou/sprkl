export default function BoxRight() {
  return (
    <svg
      className="absolute bottom-5 right-5"
      xmlns="http://www.w3.org/2000/svg"
      width="19.379"
      height="27.699"
      viewBox="0 0 19.379 27.699"
    >
      <path
        id="Path_63"
        data-name="Path 63"
        d="M967.446,490.083l-8.447-.05c3.716,4.674,7.431,9.211,11.147,13.885-3.695,4.94-6.686,8.874-10.381,13.814l8.268-.056c3.563-4.8,6.456-8.643,9.962-13.363l.382-.505L978,503.32C974.473,498.864,970.978,494.559,967.446,490.083Z"
        transform="translate(-958.999 -490.033)"
        fill="#FFFFFF"
      />
    </svg>
  );
}
