import { gsap } from "gsap/dist/gsap";
import { useEffect, useRef, useState } from "react";
export default function Email() {
  let path = useRef();
  let path2 = useRef();
  const [animation, setAnimation] = useState();

  useEffect(() => {
    setAnimation(
      gsap
        .to([path.current, path2.current], { stroke: "#FF00AA", duration: 0.3 })
        .pause()
    );
  }, []);

  return (
    <svg
      onMouseEnter={() => animation.play()}
      onMouseLeave={() => animation.reverse()}
      className="inline-block cursor-pointer stroke-current text-red"
      xmlns="http://www.w3.org/2000/svg"
      width="45.501"
      height="34.443"
      viewBox="0 0 45.501 34.443"
    >
      <g
        id="Group_104"
        data-name="Group 104"
        transform="translate(-1020.131 -371.266)"
      >
        <path
          ref={path}
          id="Path_85"
          data-name="Path 85"
          d="M1058.379,372.766h-31.443a5.256,5.256,0,0,0-5.241,5.241v20.962a5.256,5.256,0,0,0,5.241,5.241h31.443a5.256,5.256,0,0,0,5.24-5.241V378.007a5.256,5.256,0,0,0-5.24-5.241Z"
          transform="translate(0)"
          fill="none"
          stroke="#00ffe6"
          strokeMiterlimit="10"
          strokeWidth="3"
        />
        <path
          ref={path2}
          id="Path_86"
          data-name="Path 86"
          d="M1022.757,386.045l20.517,10.154,20.971-10.154"
          transform="translate(-0.617 -7.711)"
          fill="none"
          stroke="#00ffe6"
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeWidth="3"
        />
      </g>
    </svg>
  );
}
