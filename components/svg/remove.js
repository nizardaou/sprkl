export default function Remove() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="12.193"
      height="12.193"
      viewBox="0 0 12.193 12.193"
    >
      <g
        id="Group_99"
        data-name="Group 99"
        transform="translate(340.558 -703.304) rotate(45)"
      >
        <line
          id="Line_20"
          data-name="Line 20"
          y2="11.243"
          transform="translate(265.121 732.5)"
          fill="none"
          stroke="#00ffe6"
          strokeLinecap="round"
          strokeWidth="3"
        />
        <line
          id="Line_21"
          data-name="Line 21"
          y2="11.243"
          transform="translate(270.743 738.122) rotate(90)"
          fill="none"
          stroke="#00ffe6"
          strokeLinecap="round"
          strokeWidth="3"
        />
      </g>
    </svg>
  );
}
