import { gsap } from "gsap/dist/gsap";
import { useEffect, useRef, useState } from "react";
export default function Whatsapp() {
  let path = useRef();

  const [animation, setAnimation] = useState();

  useEffect(() => {
    setAnimation(
      gsap.to(path.current, { fill: "#FF00AA", duration: 0.3 }).pause()
    );
  }, []);

  return (
    <svg
      onMouseEnter={() => animation.play()}
      onMouseLeave={() => animation.reverse()}
      className="inline-block ml-4 cursor-pointer"
      xmlns="http://www.w3.org/2000/svg"
      width="37.452"
      height="37.621"
      viewBox="0 0 37.452 37.621"
    >
      <g id="logo" transform="translate(-257.792 -1096.425)">
        <g
          id="Group_103"
          data-name="Group 103"
          transform="translate(257.792 1096.425)"
        >
          <g id="Group_102" data-name="Group 102" transform="translate(0 0)">
            <path
              ref={path}
              id="Path_84"
              data-name="Path 84"
              d="M289.787,1101.892a18.649,18.649,0,0,0-29.35,22.493l-2.646,9.66,9.886-2.593a18.638,18.638,0,0,0,8.911,2.269h.008a18.649,18.649,0,0,0,13.191-31.83ZM276.6,1130.574h-.006a15.479,15.479,0,0,1-7.889-2.16l-.566-.336-5.866,1.539,1.566-5.718-.369-.586a15.5,15.5,0,1,1,13.13,7.261Zm8.5-11.6c-.466-.233-2.757-1.36-3.184-1.515s-.738-.233-1.049.233-1.2,1.516-1.475,1.827-.544.349-1.009.116a12.727,12.727,0,0,1-3.747-2.312,14.049,14.049,0,0,1-2.592-3.227c-.272-.466-.029-.718.2-.951.21-.208.466-.544.7-.816a3.174,3.174,0,0,0,.466-.777.858.858,0,0,0-.039-.816c-.116-.233-1.048-2.526-1.437-3.458-.378-.908-.762-.786-1.048-.8s-.583-.016-.893-.016a1.713,1.713,0,0,0-1.242.583,5.225,5.225,0,0,0-1.631,3.886,9.058,9.058,0,0,0,1.9,4.818c.233.311,3.286,5.016,7.96,7.033a26.747,26.747,0,0,0,2.656.981,6.389,6.389,0,0,0,2.935.185c.9-.134,2.757-1.127,3.145-2.215a3.893,3.893,0,0,0,.272-2.215C285.875,1119.319,285.564,1119.2,285.1,1118.969Z"
              transform="translate(-257.792 -1096.425)"
              fill="#00ffe6"
            />
          </g>
        </g>
      </g>
    </svg>
  );
}
