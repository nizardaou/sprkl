import { gsap } from "gsap/dist/gsap";
import {
  useEffect,
  useRef,
  useState,
  useImperativeHandle,
  forwardRef,
} from "react";
const Youtube = forwardRef((props, ref) => {
  useImperativeHandle(ref, () => ({
    animateEnter,
    animateLeave,
  }));
  let path = useRef(null);
  const [animation, setAnimation] = useState();
  function animateEnter() {
    animation.play();
  }
  function animateLeave() {
    animation.reverse();
  }
  useEffect(() => {
    setAnimation(
      gsap
        .to(path.current, {
          fill: "#FF00AA",
          duration: 0.3,
        })
        .pause()
    );
  }, []);
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="59.411"
      height="41.226"
      viewBox="0 0 59.411 41.226"
    >
      <path
        ref={path}
        id="Path_83"
        data-name="Path 83"
        d="M-123.582,3.572h-28.365c-15.523,0-15.523,4.422-15.523,15.3V29.5c0,10.343,2.2,15.3,15.523,15.3h28.365c12.036,0,15.523-2.9,15.523-15.3V18.87C-108.059,7.422-108.645,3.572-123.582,3.572Zm-20.252,28.692V15.579l16.014,8.315Z"
        transform="translate(167.47 -3.572)"
        fill="#00ffe6"
      />
    </svg>
  );
});

export default Youtube;
