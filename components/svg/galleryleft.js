export default function GalleryLeft({ moveLeft }) {
  return (
    <svg
      onClick={moveLeft}
      className="left-2 lg:left-24 top-1/2 absolute cursor-pointer"
      xmlns="http://www.w3.org/2000/svg"
      width="22.664"
      height="32.395"
      viewBox="0 0 22.664 32.395"
    >
      <path
        id="Path_72"
        data-name="Path 72"
        d="M968.878,490.091,959,490.033c4.346,5.466,8.691,10.772,13.037,16.239-4.321,5.778-7.82,10.378-12.141,16.156l9.67-.066c4.167-5.614,7.551-10.108,11.651-15.628l.447-.591-.447-.57C977.1,500.361,973.008,495.327,968.878,490.091Z"
        transform="translate(981.663 522.428) rotate(180)"
        fill="#00ffe6"
      />
    </svg>
  );
}
