import { forwardRef } from "react";

const SliderIcon = forwardRef((props, ref) => {
  return (
    <svg
      ref={ref}
      className="mx-auto w-6 h-6 lg:h-auto lg:w-auto mb-2 mt-5 lg:mt-10 lg:mb-5"
      xmlns="http://www.w3.org/2000/svg"
      width="41.434"
      height="28.988"
      viewBox="0 0 41.434 28.988"
    >
      <path
        id="Path_20"
        data-name="Path 20"
        d="M1150.374,412.765l-12.635-.075c5.558,6.992,11.116,13.779,16.674,20.77-5.526,7.39-10,13.274-15.528,20.664l12.368-.084c5.33-7.18,9.657-12.929,14.9-19.989l.572-.756-.572-.729C1160.885,425.9,1155.656,419.461,1150.374,412.765Z"
        transform="translate(454.124 -1137.738) rotate(90)"
        fill="#00ffe6"
      />
    </svg>
  );
});

export default SliderIcon;
