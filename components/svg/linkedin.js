import { gsap } from "gsap/dist/gsap";
import {
  useEffect,
  useRef,
  useState,
  useImperativeHandle,
  forwardRef,
} from "react";
const linkedIn = forwardRef((props, ref) => {
  useImperativeHandle(ref, () => ({
    animateEnter,
    animateLeave,
  }));
  let path1 = useRef();
  let path2 = useRef();
  let path3 = useRef();
  const [animation, setAnimation] = useState();
  function animateEnter() {
    animation.play();
  }
  function animateLeave() {
    animation.reverse();
  }
  useEffect(() => {
    setAnimation(
      gsap
        .to([path1.current, path2.current, path3.current], {
          fill: "#FF00AA",
          duration: 0.3,
        })
        .pause()
    );
  }, []);
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="43.412"
      height="41.413"
      viewBox="0 0 43.412 41.413"
    >
      <g id="Group_85" data-name="Group 85" transform="translate(0)">
        <path
          ref={path1}
          id="Path_77"
          data-name="Path 77"
          d="M-1532.366,1336.581v16h-9.364v-14.91a9.085,9.085,0,0,0-1.045-4.615,3.771,3.771,0,0,0-3.544-1.794,4.483,4.483,0,0,0-3.161,1.068,6.925,6.925,0,0,0-1.7,2.339,2.4,2.4,0,0,0-.226,1.071v16.841h-9.273v-28h9.273v4a.12.12,0,0,0-.137.134h.137v-.134a15.478,15.478,0,0,1,2.773-3.047q1.815-1.591,5.637-1.591a10.261,10.261,0,0,1,7.637,3.047Q-1532.366,1330.036-1532.366,1336.581Z"
          transform="translate(1575.778 -1311.17)"
          fill="#00ffe6"
        />
        <path
          ref={path2}
          id="Path_78"
          data-name="Path 78"
          d="M-1683.672,1215.7a4.373,4.373,0,0,1-1.5,3.364,5.461,5.461,0,0,1-3.818,1.362,5.243,5.243,0,0,1-3.841-1.362,4.532,4.532,0,0,1-1.388-3.364,4.406,4.406,0,0,1,1.479-3.455,5.62,5.62,0,0,1,3.887-1.319,5.051,5.051,0,0,1,3.59,1.319A5.15,5.15,0,0,1-1683.672,1215.7Z"
          transform="translate(1694.219 -1210.926)"
          fill="#00ffe6"
        />
        <rect
          ref={path3}
          id="Rectangle_136"
          data-name="Rectangle 136"
          width="9.362"
          height="28.002"
          transform="translate(0.503 13.411)"
          fill="#00ffe6"
        />
      </g>
    </svg>
  );
});

export default linkedIn;
