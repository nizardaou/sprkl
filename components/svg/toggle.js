import { forwardRef } from "react";

const Toggle = forwardRef((props, ref) => {
  return (
    <svg
      ref={ref}
      className="w-full"
      xmlns="http://www.w3.org/2000/svg"
      width="32.395"
      height="22.664"
      viewBox="0 0 32.395 22.664"
    >
      <path
        id="Path_81"
        data-name="Path 81"
        d="M968.878,490.091,959,490.033c4.346,5.466,8.691,10.772,13.037,16.239-4.321,5.778-7.82,10.378-12.141,16.156l9.67-.066c4.167-5.614,7.551-10.108,11.651-15.628l.447-.591-.447-.57C977.1,500.361,973.008,495.327,968.878,490.091Z"
        transform="translate(522.428 -958.999) rotate(90)"
        fill="#fff"
      />
    </svg>
  );
});

export default Toggle;
