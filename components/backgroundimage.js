export default function BackgroundImage({ children, ...props }) {
  return (
    <div className="max-w-full relative bg-cover flex justify-end mt-36">
      <img
        height={800}
        width={1920}
        className="max-w-full lg:h-auto object-cover h-400"
        src={`/images/Banners/${props.image}`}
      />
      <h2 className="text-xl absolute lg:text-4xl w-1/2 lg:w-1/4 mr-10 lg:mr-56 py-40 text-white font-futuramaxi font-normal">
        {children}
      </h2>
    </div>
  );
}
