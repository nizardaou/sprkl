import { useEffect, useRef, useState } from "react";
import { gsap, Power2 } from "gsap/dist/gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import AnimatedLink from "./animatedlink";
import { useRouter } from "next/router";
import Logo from "./svg/logo";

gsap.registerPlugin(ScrollTrigger);

const navigation = [
  { name: "about", href: "/about" },
  { name: "focus", href: "/focus" },
  { name: "work", href: "/work" },
  { name: "contact", href: "/contact" },
];

export default function Header() {
  let router = useRouter();
  let menu = useRef(null);
  let mobileMenu = useRef(null);
  const links = useRef([]);
  links.current = [];
  const top = useRef();
  const bot = useRef();
  const [topToggle, setTopToggle] = useState();
  const [bottomToggle, setBottomToggle] = useState();
  const [opened, setOpened] = useState(false);
  const [mobileMenuAnimation, setMobileMenuAnimation] = useState();

  useEffect(() => {
    //toggle animation states declaration
    setTopToggle(
      gsap
        .to(top.current, {
          rotate: "45",
          width: "2rem",
          translateY: "0",
          duration: 0.5,
          ease: Power2.easeIn,
        })
        .pause()
    );
    setBottomToggle(
      gsap
        .to(bot.current, {
          rotate: "-45",
          width: "2rem",
          translateY: "0",
          duration: 0.5,
          ease: Power2.easeIn,
        })
        .pause()
    );

    //scroll menu animation
    ScrollTrigger.create({
      id: "menu1",
      start: 0,
      end: 99999,
      scrub: 1,
      onUpdate: (self) => {
        if (self.direction === 1) {
          gsap.to(menu.current, {
            top: "-150px",
            duration: 0.5,
            ease: "none",
          });
        } else if (self.direction === -1 && self.progress > 0) {
          gsap.to(menu.current, { backgroundColor: "rgb(28,29,31,0.9)" });
          gsap.to(menu.current, {
            top: "0px",
            duration: 0.5,
            ease: "none",
          });
        } else if (self.direction === -1 && self.progress === 0) {
          gsap.to(menu.current, {
            backgroundColor: "rgb(28,29,31,0)",
            ease: "none",
          });
        }
      },
    });
    if (mobileMenu.current) {
      setMobileMenuAnimation(
        gsap
          .to(mobileMenu.current, { right: 0, left: 0, duration: 0.5 })
          .pause()
      );
    }
  }, []);

  useEffect(() => {
    //play menu animation on mount
    if (scrollY === 0) {
      gsap
        .to(menu.current, {
          top: 0,
          duration: 1,
          ease: "fade in",
        })
        .play();
    } else {
      gsap
        .to(menu.current, {
          top: 0,
          backgroundColor: "rgb(28,29,31,0.9)",
          duration: 1,
          ease: "fade in",
        })
        .play();
    }
  }, [router.asPath]);

  function handleMobileMenu(page) {
    if (mobileMenuAnimation) {
      setOpened(!opened);
      if (!opened && page !== "home") {
        mobileMenuAnimation.play();
        bottomToggle.play();
        topToggle.play();
      } else {
        mobileMenuAnimation.reverse();
        bottomToggle.reverse();
        topToggle.reverse();
      }
    }
  }

  return (
    <>
      <div
        ref={menu}
        className="z-50 fixed bg-transparent -top-36 left-0 w-screen pt-7 pb-7 "
      >
        <nav
          className="flex items-center lg:max-w-7xl w-5/6 mx-auto justify-between sm:h-10 lg:justify-start"
          aria-label="Global"
        >
          <div className="flex items-center flex-grow flex-shrink-0 lg:flex-grow-0">
            <div className="flex items-center justify-between w-full md:w-auto">
              <a
                className="cursor-pointer"
                onClick={() => {
                  handleMobileMenu("home");
                  router.push("/", undefined, { scroll: false });
                }}
              >
                <span className="sr-only">Workflow</span>
                <Logo />
              </a>

              <div className=" flex items-center md:hidden">
                <button
                  onClick={() => {
                    handleMobileMenu();
                  }}
                  className="bg-transparent relative pointer-events-auto focus:outline-none"
                >
                  <span className="sr-only">Open main menu</span>
                  <span
                    ref={top}
                    className="w-12 block absolute right-0 h-1 bg-white transform origin-center -translate-y-3"
                  />
                  <span
                    ref={bot}
                    className="w-7 block absolute right-0 h-1 bg-white transform origin-center translate-y-1"
                  />
                </button>
              </div>
            </div>
          </div>
          <div className="hidden font-bold md:block md:ml-auto md:space-x-10 text-white text-xl">
            {navigation.map((item, index) => (
              <AnimatedLink
                extraClasses="font-futuramaxi"
                type="menu"
                key={index}
                href={item.href}
              >
                {item.name}
              </AnimatedLink>
            ))}
          </div>
        </nav>
      </div>

      <div
        ref={mobileMenu}
        className="fixed z-40 top-0 -right-96 p-0 h-screen md:hidden"
      >
        <div className="h-full shadow-md bg-primary bg-opacity-95 overflow-hidden">
          <div className="px-2 font-futuramaxi flex flex-col justify-center  items-center h-full font-light pt-2 pb-3 space-y-1">
            {navigation.map((item) => (
              <a
                onClick={() => {
                  handleMobileMenu();
                  router.push(item.href, undefined, { scroll: false });
                }}
                key={item.name}
                className=" px-3 py-2 text-3xl cursor-pointer rounded-md font-medium text-white"
              >
                {item.name}
              </a>
            ))}
          </div>
        </div>
      </div>
    </>
  );
}
