import { forwardRef } from "react";
const HeadingImage = forwardRef((props, ref) => {
  const { heading, image } = props;
  return (
    <>
      <div
        ref={ref}
        className="relative mx-auto mt-24 md:mt-32 lg:mt-44 w-screen"
      >
        <div className="absolute w-full top-9 lg:top-14">
          <h1
            className={`text-4xl w-5/6 mx-auto md:text-5xl lg:max-w-7xl text-white lg:text-7xl font-futuramaxi font-normal ${
              heading.includes("decommoditising")
                ? "hidden sm:block"
                : ""
            }`}
          >
            {heading}
          </h1>
          {heading.includes("decommoditising") && (
            <h1 className="text-4xl sm:hidden w-5/6 mx-auto md:text-5xl lg:max-w-7xl text-white lg:text-7xl font-futuramaxi font-normal">
              <span>de-</span> <br />
              commoditising gaming in africa
            </h1>
          )}
        </div>
        <img
          height={800}
          width={1920}
          className="max-w-full lg:h-auto object-cover h-400"
          src={image}
        />
      </div>
    </>
  );
});

export default HeadingImage;
