import { useEffect, useState, useRef } from "react";
import { gsap } from "gsap/dist/gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import { useRouter } from "next/router";
import AnimatedLink from "./animatedlink";

gsap.registerPlugin(ScrollTrigger);

export default function Item(props) {
  const { project, level, carouselLeft, carouselRight } = props;

  let video = useRef();
  let router = useRouter();
  const [inView, setInView] = useState(false);
  const [transitioned, setTransitoned] = useState(false);

  function handleClick(link, title) {
    if (level === -1) {
      carouselRight();
    } else if (level === 1) {
      carouselLeft();
    } else if (level == 0) {
      if (title) {
        router.push(link, undefined, { scroll: false });
      } // if title doesn't exist then we are on work pages carousel
      else if (!title && link != "#") {
        // creating a link element for e-chir deck
        if (link.includes("pdf")) {
          const link = document.createElement("a");
          link.setAttribute("download", "e-chir.pdf");
          link.href = "/documents/e-chir.pdf";
          document.body.appendChild(link);
          link.click();
          document.body.removeChild(link);
        } else window.open(link, "_blank");
      }
    }
  }

  useEffect(() => {
    setTransitoned(true);
  }, []);

  useEffect(() => {
    setTransitoned(false);
    setTimeout(() => setTransitoned(true), 500);

    ScrollTrigger.config({ limitCallbacks: true });
    if (level === 0 && video.current) {
      ScrollTrigger.create({
        id: `video-${level}`,
        trigger: video.current,
        start: "top center+=250",
        onEnter: () => {
          if (video.current) video.current.play();
          setInView(true);
        },
        onLeave: () => {
          if (video.current) video.current.pause();
          setInView(false);
        },
        onEnterBack: () => {
          if (video.current) video.current.play();
          setInView(true);
        },
        onLeaveBack: () => {
          if (video.current) video.current.pause();
          setInView(false);
        },
      });
    } else {
      if (video.current) video.current.pause();
    }

    return () => {
      if (ScrollTrigger.getById(`video-${level}`))
        ScrollTrigger.getById(`video-${level}`).kill();
    };
  }, [level]);

  useEffect(() => {
    if (inView) {
      if (level === 0) {
        video.current.play();
      } else {
        video.current.pause();
      }
    }
  }, [level, inView]);

  return (
    <div className={"absolute text-white item level" + level}>
      {project.type === "video" ? (
        <video
          onClick={() => handleClick(project.href, project.title)}
          ref={video}
          width={800}
          height={800}
          preload="auto"
          autoPlay={false}
          loop="loop"
          className={` ${
            level === 1 || level === -1
              ? "cursor-pointer"
              : project.href !== "#" && level === 0 && transitioned
              ? "open"
              : ""
          }`}
          muted
          playsInline
        >
          <source src={project.asset + ".webm"} type="video/webm" />
          <source src={project.asset + ".mp4"} type="video/mp4" />
          Your browser does not support the video tag.
        </video>
      ) : project.type === "image" ? (
        <img
          className={`max-w-full h-auto ${
            level === 1 || level === -1
              ? "cursor-pointer"
              : project.href !== "#" && level === 0 && transitioned
              ? "open"
              : ""
          }`}
          src={project.asset}
          width={800}
          height={800}
          onClick={() => handleClick(project.href, project.title)}
        />
      ) : null}
      {project.title && (
        <div className="absolute top-5 left-5 lg:top-10 lg:left-10 z-50">
          <AnimatedLink
            type="carousel"
            href={project.href}
            extraClasses="text-4xl sm:text-5xl md:text-6xl lg:text-7xl font-futuramaxi font-normal py-2"
            extraContainerClass="mx-auto"
          >
            {project.title}
          </AnimatedLink>
        </div>
      )}
      {project.href !== "#" && level === 0 && (
        <img
          src="/images/open.png"
          className="md:hidden absolute right-5 bottom-5 test"
        />
      )}
    </div>
  );
}
