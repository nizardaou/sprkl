import { useRef } from "react";
import { gsap } from "gsap/dist/gsap";
import BoxRight from "./svg/boxright";
import { useRouter } from "next/router";

export default function AnimatedBox(props) {
  let router = useRouter();
  let internal = useRef(null);

  function mouseEnter() {
    gsap.to(internal.current, {
      opacity: 1,
      backgroundColor: "rgba(28, 29, 31, 0.9)",
      duration: 0.2,
    });
  }
  function mouseLeave() {
    gsap.to(internal.current, { opacity: 0, duration: 0.5 });
  }

  return (
    <div
      className="bg-box relative cursor-pointer bg-cover"
      onClick={() =>
        router.push("/work/" + props.link, undefined, { scroll: false })
      }
    >
      <img
        alt={props.name}
        src={props.logo}
        width={370}
        height={300}
        className="h-auto w-1/2 max-w-full absolute right-0 top-0 bottom-0 left-0 m-auto"
      />
      <div
        ref={internal}
        onMouseEnter={() => {
          if (window.innerWidth > 768) mouseEnter();
        }}
        onMouseLeave={() => {
          if (window.innerWidth > 768) mouseLeave();
        }}
        className="font-futuramaxi relative py-10 pl-10 pr-10 flex flex-col h-full opacity-0"
      >
        <h2 className="text-4xl text-white">{props.name}</h2>
        <p className="text-cyan mt-5">focus:</p>
        <p className="font-light text-white">{props.focus}</p>
        <p className="text-cyan mt-5">competence:</p>
        <div className="flex justify-start flex-row flex-wrap mb-5">
          {props.competence.map((item, index) => {
            return (
              <p key={index} className="font-light text-white mr-1">
                {item + `${index + 1 !== props.competence.length ? "," : ""}`}
              </p>
            );
          })}
        </div>

        <BoxRight />
      </div>
    </div>
  );
}
