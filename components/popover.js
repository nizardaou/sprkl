export default function PopOver({ list, selectedItems, addItem }) {
  return (
    <div className="absolute w-full h-auto py-3 shadow-xl top-100 bg-dropdown bg-opacity-90 text-white z-40 left-0 rounded-lg max-h-select">
      <div className="flex flex-col w-full">
        {list.map((item, key) => {
          return (
            <div
              key={key}
              className={
                selectedItems.includes(item)
                  ? "cursor-pointer w-full text-cyan"
                  : " cursor-pointer w-full"
              }
              onClick={(e) => {
                e.stopPropagation();
                addItem(item);
              }}
            >
              <div className="flex w-full items-center p-2 pl-2 border-transparent relative">
                <div className="w-full items-center flex">
                  <div className="mx-2 text-xl font-futuramaxi">{item}</div>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
}
