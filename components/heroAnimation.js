import Lottie from "react-lottie";
import animationData from "../components/lotties/animationnew.json";
import animationDataMobile from "../components/lotties/animationmobile.json";
import { useEffect, useRef, useState } from "react";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import { gsap, Power2 } from "gsap/dist/gsap";

gsap.registerPlugin(ScrollTrigger);

const heroLayers = [
  { image: "/images/hero/L0.jpg", depth: 0.8, z: 0 },
  { image: "/images/hero/L1.png", depth: 0.2, z: 0 },
  { image: "/images/hero/L3.png", depth: 0.4, z: 0 },
  { image: "/images/hero/L4.png", depth: 0.5, z: 0 },
  { image: "/images/hero/L2.png", depth: 0.8, z: 0 },
  { image: "/images/hero/L5.png", depth: 0.8, z: 5 },
  { image: "/images/hero/L6.png", depth: 0.8, z: 5 },
  { image: "/images/hero/L7.png", depth: 0.8, z: 0 },
];

export default function HeroAnimation() {
  // default option for lottie
  const defaultOptions = {
    loop: false,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };

  // mobile options for lottie
  const mobileOptions = {
    loop: false,
    autoplay: true,
    animationData: animationDataMobile,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };

  const [digitalAnimation, setDigitalAnimation] = useState();
  const [playOne, setPlayOne] = useState(false);
  let digital = useRef(null);
  let hero = useRef(null);
  let layers = gsap.utils.selector(hero);
  let tl = useRef(null);
  let heroTl = useRef(null);

  useEffect(() => {
    // 3D parallax animation
    heroTl.current = gsap.timeline({
      scrollTrigger: {
        // markers: true,
        trigger: hero.current,
        start: "top top",
        end: "bottom top",
        onLeave: () => {
          setPlayOne(false);
          if (tl.current) tl.current.pause(0);
        },
        onEnterBack: () => setTimeout(() => setPlayOne(true), 1000),
        scrub: true,
      },
    });

    layers(".layer").forEach((layer) => {
      const depth = layer.dataset.depth;
      const movement = -(layer.offsetHeight * depth);

      heroTl.current.to(layer, { y: movement, ease: "none" }, 0);
    });

    // setTimeout to mount first lottie animation
    setTimeout(() => {
      setPlayOne(true);
    }, 1000);

    tl.current = gsap
      .timeline()
      .to(digital.current, { opacity: 1, ease: Power2.easeIn })
      .fromTo(
        digital.current,
        {
          color: "#fff",
          backgroundPosition: "200%",
          backgroundSize: "200%",
        },
        {
          backgroundImage:
            "linear-gradient(to left, rgba(0,255,230,1) 46%, rgba(255,0,170,1) 56%)",
          backgroundPosition: "100%",
          color: "transparent",
          backgroundSize: "300%",
          duration: 1,
        }
      )
      .to(digital.current, {
        color: "#fff",
      })
      .pause();

    // create @digital hover animation
    setDigitalAnimation(
      gsap
        .fromTo(
          digital.current,
          { color: "#fff", backgroundPosition: "200%", backgroundSize: "200%" },
          {
            backgroundImage:
              "linear-gradient(to left,rgba(0,255,230,1) 46%, rgba(255,0,170,1) 56%)",
            backgroundPosition: "100%",
            color: "transparent",
            backgroundSize: "300%",
            duration: 1,
          }
        )
        .pause()
    );
  }, []);

  // digital animation based on mouse actions
  function animateDigital(status) {
    if (status === "mouseEnter" && digitalAnimation) {
      digitalAnimation.restart();
    } else if (status === "mouseLeave" && digitalAnimation) {
      digitalAnimation.reverse();
    }
  }
  // digital animation timeline function
  function digitalTimeline() {
    tl.current.play();
  }

  return (
    <>
      <div ref={hero} className="relative h-screen w-screen">
        {heroLayers.map((item, index) => (
          <div
            key={index}
            className={`layer fixed bg-auto md:bg-cover md:bg-hero bg-no-repeat h-screen w-screen ${
              index === heroLayers.length - 2
                ? "bg-hero-mobile"
                : "bg-hero-bottom"
            }`}
            data-depth={item.depth}
            style={{
              zIndex: item.z,
              backgroundImage: `url(${item.image})`,
              pointerEvents: "none",
            }}
          />
        ))}
      </div>
      <main className="fixed h-screen w-full top-0 right-0 left-0 bottom-0 sm:m-auto flex flex-col mt-48 sm:mt-0 justify-start sm:justify-center lg:max-w-7xl">
        {playOne && (
          <>
            <div className="hidden sm:block">
              <Lottie
                options={defaultOptions}
                eventListeners={[
                  {
                    eventName: "complete",
                    callback: () => {
                      digitalTimeline();
                    },
                  },
                ]}
              />
            </div>
            <div className="sm:hidden">
              <Lottie
                options={mobileOptions}
                eventListeners={[
                  {
                    eventName: "complete",
                    callback: () => {
                      digitalTimeline();
                    },
                  },
                ]}
              />
            </div>
          </>
        )}
        <div className="relative mt-2 text-center">
          <a
            href="/about"
            ref={digital}
            onMouseOver={() => animateDigital("mouseEnter")}
            onMouseLeave={() => animateDigital("mouseLeave")}
            className="font-futuramaxi font-normal py-3 opacity-0 text-2xl sm:text-3xl md:text-4xl lg:text-5xl pr-0 lg:pr-0 cursor-pointer bg-clip-text"
          >
            <span className="font-sans font-bold">@</span>
            DigitalSpeed
          </a>
        </div>
      </main>
    </>
  );
}
