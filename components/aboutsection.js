import { gsap, Power2 } from "gsap/dist/gsap";
import { useEffect, useRef, forwardRef } from "react";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
gsap.registerPlugin(ScrollTrigger);

const AboutSection = forwardRef((props, ref) => {
  const { aboutItem, direction, lastItem, firstItem } = props;

  let year = useRef(null);
  let info = useRef(null);
  let details = useRef(null);
  let image = useRef(null);
  let company = useRef(null);

  const firstDivClasses =
      direction == 0
        ? "order-1 justify-end pr-10 md:pr-20 lg:pr-20"
        : "order-2 justify-start pl-10 md:pl-20 lg:pl-20",
    secondDivClasses =
      direction == 0
        ? "order-2 items-start pl-10 md:pl-20 lg:pl-20"
        : "order-1 items-end pr-10 md:pr-20 lg:pr-20",
    textAlign =
      direction === 0 ? "text-right lg:text-left" : "text-left lg:text-right",
    itemClasses = lastItem ? "mt-36" : firstItem ? "mb-36" : "my-36",
    initialYearX = direction === 0 ? 150 : -150,
    initialInfoX = direction === 0 ? -150 : 150;

  const mergeRefs = (...refs) => {
    const filteredRefs = refs.filter(Boolean);
    if (!filteredRefs.length) return null;
    if (filteredRefs.length === 0) return filteredRefs[0];
    return (inst) => {
      for (const ref of filteredRefs) {
        if (typeof ref === "function") {
          ref(inst);
        } else if (ref) {
          ref.current = inst;
        }
      }
    };
  };

  useEffect(() => {
    let startValue;

    if (firstItem) {
      startValue = 10;
    } else {
      startValue = "top center+=250";
    }

    gsap.fromTo(
      year.current,
      {
        autoAlpha: 0,
      },
      {
        duration: 1,
        autoAlpha: 1,
        ease: Power2.easeIn,
        scrollTrigger: {
          id: "yearFading",
          trigger: year.current,
          start: startValue,
          toggleActions: "play none none reverse",
        },
      }
    );

    gsap.fromTo(
      year.current,
      {
        xPercent: initialYearX,
      },
      {
        duration: 0.5,
        xPercent: 0,
        ease: Power2.easeIn,
        scrollTrigger: {
          id: "yearPlacement",
          trigger: year.current,
          start: startValue,
          toggleActions: "play none none reverse",
        },
      }
    );

    gsap.fromTo(
      image.current,
      {
        autoAlpha: 0,
      },
      {
        duration: 1,
        autoAlpha: 1,
        ease: Power2.easeIn,
        scrollTrigger: {
          id: "imageFading",
          trigger: year.current,
          start: startValue,
          toggleActions: "play none none reverse",
        },
      }
    );

    gsap.fromTo(
      image.current,
      {
        xPercent: initialInfoX,
      },
      {
        duration: 0.5,
        xPercent: 0,
        ease: Power2.easeIn,
        scrollTrigger: {
          id: "imagePlacing",
          trigger: year.current,
          start: startValue,
          toggleActions: "play none none reverse",
        },
      }
    );

    gsap.fromTo(
      details.current,
      {
        autoAlpha: 0,
      },
      {
        // immediateRender: false,
        duration: 1,
        ease: Power2.easeIn,
        autoAlpha: 1,
        scrollTrigger: {
          id: "detailsFading",
          trigger: year.current,
          start: startValue,
          toggleActions: "play none none reverse",
        },
      }
    );

    gsap.fromTo(
      details.current,
      {
        xPercent: initialInfoX,
      },
      {
        // immediateRender: false,
        duration: 0.5,
        xPercent: 0,
        ease: Power2.easeIn,
        scrollTrigger: {
          id: "detailsPlacement",
          trigger: year.current,
          start: startValue,
          toggleActions: "play none none reverse",
        },
      }
    );

    gsap.fromTo(
      company.current,
      {
        autoAlpha: 0,
      },
      {
        // immediateRender: false,
        duration: 1,
        autoAlpha: 1,
        ease: Power2.easeIn,
        scrollTrigger: {
          id: "companyFading",
          trigger: year.current,
          start: startValue,
          toggleActions: "play none none reverse",
        },
      }
    );

    gsap.fromTo(
      company.current,
      {
        xPercent: initialInfoX,
      },
      {
        // immediateRender: false,
        duration: 0.5,
        xPercent: 1,
        ease: Power2.easeIn,
        scrollTrigger: {
          id: "companyPlacement",
          trigger: year.current,
          start: startValue,
          toggleActions: "play none none reverse",
        },
      }
    );

    return () =>
      ScrollTrigger.getAll().forEach((t) => {
        if (t.vars.id != "menu1") t.kill();
      });
  }, []);

  return (
    <div
      className={
        "overflow-hidden relative grid grid-cols-2 gap-0 " + " " + itemClasses
      }
    >
      <div
        className={
          firstDivClasses + " " + "flex items-center overflow-hidden max-w-full"
        }
      >
        <img
          width={250}
          height={105}
          ref={mergeRefs(year, ref)}
          src={aboutItem.year}
          className="opacity-0 max-w-full h-auto"
        ></img>
      </div>
      <div
        ref={info}
        className={
          secondDivClasses +
          " " +
          "font-merriweather text-white flex flex-col overflow-hidden"
        }
      >
        <img
          ref={image}
          className="max-w-full h-auto opacity-0"
          width={aboutItem.imageWidth}
          height={aboutItem.imageHeight}
          src={aboutItem.image}
        />

        <p
          dangerouslySetInnerHTML={{ __html: aboutItem.details }}
          ref={details}
          className={
            textAlign +
            " " +
            "text-lg md:text-2xl lg:text-4xl mt-10 max-w-full opacity-0 inline-block"
          }
        ></p>

        <p
          ref={company}
          className={
            textAlign + " " + "text-cyan w-full mt-2 max-w-full opacity-0"
          }
        >
          {aboutItem.company}
        </p>
      </div>
    </div>
  );
});

export default AboutSection;
