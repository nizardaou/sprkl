export default function headingtext({ children, ...props }) {
  const { heading } = props;
  return (
    <>
      <div className="bg-primary relative">
        <div className="lg:max-w-7xl w-5/6 mx-auto  bg-primary relative pt-10 md:pt-16 lg:pt-24">
          <h2 className="text-3xl md:text-5xl lg:text-6xl mb-5 lg:mb-0 font-merriweather font-normal text-cyan">
            {heading}
          </h2>
          <p
            className={`text-white leading-normal lg:pt-12 text-lg md:text-xl lg:text-custom-3xl font-merriweather font-extralight ${
              heading === "solutions" ? "mb-16 lg:mb-24" : "mb-0"
            }`}
          >
            {children}
          </p>
        </div>
      </div>
    </>
  );
}
