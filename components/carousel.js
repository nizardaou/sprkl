import React, { useState, useImperativeHandle, forwardRef } from "react";
import Item from "./item";
import SliderItem from "./slideritem";
import { TransitionGroup, CSSTransition } from "react-transition-group";

const Carousel = forwardRef((props, ref) => {
  const { active, items, classes, animateCarousel, animateImage } = props;
  const [carouselState, setCarouselState] = useState({
    active: active,
    direction: "",
  });

  const [transitioned, setTransitioned] = useState(true);
  const [firstRenderTop, setFirstRenderTop] = useState(true);
  const [touchStart, setTouchStart] = useState(0);
  const [scrollStart, setScrollStart] = useState(0);
  const [touchEnd, setTouchEnd] = useState(0);

  // The component instance will be extended
  // with whatever you return from the callback passed
  // as the second argument
  useImperativeHandle(ref, () => ({
    moveRight,
    moveLeft,
  }));

  function carouselContent(link, linkHref, subtitle, image) {
    animateCarousel(link, linkHref, subtitle);
    if (image) animateImage(image);
  }

  function generateItems() {
    var elements = [];
    var level;
    // console.log(carouselState);
    for (var i = carouselState.active - 1; i < carouselState.active + 2; i++) {
      var index = i;
      // console.log(i, index);
      if (i < 0) {
        index = items.length + i;
      } else if (i >= items.length) {
        index = i % items.length;
      }
      level = carouselState.active - i;

      if (classes === "slider") {
        elements.push(
          <CSSTransition key={index} timeout={{ enter: 500, exit: 400 }}>
            <SliderItem
              project={items[index]}
              level={level}
              carouselLeft={moveLeft}
              carouselRight={moveRight}
              carouselContent={carouselContent}
              firstRenderTop={firstRenderTop}
            />
          </CSSTransition>
        );
      } else
        elements.push(
          <CSSTransition
            component={null}
            key={index}
            timeout={{ enter: 500, exit: 400 }}
          >
            <Item
              project={items[index]}
              level={level}
              carouselLeft={moveLeft}
              carouselRight={moveRight}
            />
          </CSSTransition>
        );
    }
    return elements;
  }

  function moveLeft() {
    if (transitioned) {
      setTransitioned(false);
      setTimeout(() => {
        setTransitioned(true);
      }, 500);
      var newActive = carouselState.active;
      newActive--;
      setCarouselState({
        active: newActive < 0 ? items.length - 1 : newActive,
        direction: "left",
      });
    }
  }

  function moveRight(status) {
    if (status === "notFirstRender") setFirstRenderTop(false);
    if (transitioned) {
      setTransitioned(false);
      setTimeout(() => {
        setTransitioned(true);
      }, 500);
      var newActive = carouselState.active;
      setCarouselState({
        active: (newActive + 1) % items.length,
        direction: "right",
      });
    }
  }

  function handleTouchStart(e) {
    setScrollStart(window.scrollY);
    setTouchStart(e.targetTouches[0].clientX);
  }

  function handleTouchMove(e) {
    if (
      touchStart - e.targetTouches[0].clientX > 10 &&
      e.targetTouches[0].clientX !== 0 &&
      window.scrollY === scrollStart
    ) {
      moveRight();
    }

    if (
      touchStart - e.targetTouches[0].clientX < -10 &&
      e.targetTouches[0].clientX !== 0 &&
      window.scrollY === scrollStart
    ) {
      moveLeft();
    }
  }

  function handleTouchEnd() {
    setTouchStart(0);
  }

  return (
    <>
      <div
        ref={ref}
        onTouchEnd={handleTouchEnd}
        onTouchStart={handleTouchStart}
        onTouchMove={handleTouchMove}
        className={`relative select-none ${
          classes === ""
            ? "lg:max-w-7xl w-5/6 mx-auto"
            : classes === "slider"
            ? "mx-auto w-screen lg:max-w-7xl"
            : ""
        }
       `}
      >
        {classes === "" && (
          <img
            height={800}
            width={800}
            src="/work/E-CHIR/Gallery/1.jpg"
            className="invisible"
          />
        )}
        <TransitionGroup
          childFactory={(child) =>
            React.cloneElement(child, {
              classNames: carouselState.direction + classes,
            })
          }
        >
          {generateItems()}
        </TransitionGroup>
      </div>
    </>
  );
});

export default Carousel;
