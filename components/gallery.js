import { useState } from "react";
import GalleryRight from "./svg/galleryright";
import GalleryLeft from "./svg/galleryleft";

export default function WorkCarousel(props) {
  const { items, active } = props;

  const [carouselState, setCarouselState] = useState({
    active: active,
    direction: "",
  });

  function generateItems() {
    var elements = [];
    var level;
    // console.log(carouselState);
    for (var i = carouselState.active - 2; i < carouselState.active + 3; i++) {
      var index = i;
      if (i < 0) {
        index = items.length + i;
      } else if (i >= items.length) {
        index = i % items.length;
      }
      level = carouselState.active - i;

      elements.push(
        <img
          className={level == 0 ? "" : "hidden"}
          key={index}
          src={items[index].image}
          alt={items[index].title}
        ></img>
      );
    }
    return elements;
  }

  function moveLeft() {
    var newActive = carouselState.active;
    newActive--;
    setCarouselState({
      active: newActive < 0 ? items.length - 1 : newActive,
    });
  }

  function moveRight() {
    var newActive = carouselState.active;
    setCarouselState({
      active: (newActive + 1) % items.length,
    });
  }
  return (
    <div className="relative mt-24">
      <GalleryRight moveRight={moveRight} />
      <GalleryLeft moveLeft={moveLeft} />
      <div className="lg:max-w-7xl w-5/6 mx-auto select-none">
        {generateItems()}
      </div>
      <div className="lg:max-w-7xl mx-auto w-5/6 grid grid-cols-3 lg:grid-cols-5 gap-0 select-none">
        {items.map((item, index) => {
          return (
            <img
              className="cursor-pointer h-44"
              key={index}
              src={item.image}
              onClick={() =>
                setCarouselState({
                  active: index,
                })
              }
            ></img>
          );
        })}
      </div>
    </div>
  );
}
