import { forwardRef } from "react";

const Section = forwardRef((props, ref) => {
  return (
    <div
      ref={ref}
      className={
        `relative lg:pl-0 bg-primary ${
          props.flip ? " text-cyan" : "text-white"
        }` +
        " " +
        props.extraClasses
      }
    >
      <div className="lg:max-w-7xl w-5/6 mx-auto bg-primary">
        <div className="relative z-10 bg-primary lg:max-w-full lg:w-full">
          <h1 className="text-4xl md:text-6xl xl:text-7xl font-futuramaxi font-normal">
            <span className={` ${props.flip ? "text-white" : "text-cyan"}`}>
              {props.coloredHeading}
            </span>{" "}
            {props.whiteHeading}
          </h1>

          <p
            className={
              "text-xl md:text-2xl xl:text-custom-3xl leading-relaxed font-merriweather text-white font-extralight max-w-4xl" +
              " " +
              props.extraChildrenClasses
            }
          >
            {props.children}
          </p>
        </div>
      </div>
    </div>
  );
});

export default Section;
