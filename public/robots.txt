# *
User-agent: *
Allow: /

# Host
Host: https://sprkl.co

# Sitemaps
Sitemap: https://sprkl.co/sitemap.xml
