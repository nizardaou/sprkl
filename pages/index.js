import Head from "next/head";
import { gsap, Power2, Bounce } from "gsap/dist/gsap";
import Section from "../components/section";
import { useEffect, useRef, useState } from "react";
import HeroAnimation from "../components/heroAnimation";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import Carousel from "../components/carousel";
import { TransitionGroup, CSSTransition } from "react-transition-group";
import SliderIcon from "../components/svg/slidericon";
import AnimatedLink from "../components/animatedlink";

gsap.registerPlugin(ScrollTrigger);

const carouselItems = [
  {
    title: "F&B",
    asset: "/videos/FNB",
    href: "/focus#f&b",
    type: "video",
  },
  {
    title: "healthcare",
    asset: "/videos/Healthcare",
    href: "/focus#healthcare",
    type: "video",
  },
  {
    title: "egaming",
    asset: "/videos/eGaming",
    href: "/focus#egaming",
    type: "video",
  },
  {
    title: "public & ngo",
    asset: "/videos/Ngo",
    href: "/focus#public&ngo",
    type: "video",
  },
];

const sliderItems = [
  {
    title: "tech",
    subtitle: "we create customer-centric high-performance digital platforms.",
    image: "/images/Competence/Tech.jpg",
    link: "See what we did for NABAD",
    linkHref: "/work/NGO-marketing-nabad",
    href: "/work#tech",
  },
  {
    title: "insights",
    subtitle:
      "we turn data into insightful and actionable business intelligence.",
    image: "/images/Competence/Insights.jpg",
    link: "See what we did for the EU",
    linkHref: "/work/public-sector-marketing-strategy-eu",
    href: "/work#insights",
  },
  {
    title: "brand",
    subtitle: "we design brand identities that reflect business visions.",
    image: "/images/Competence/Brand.jpg",
    link: "See what we did for Afriplay",
    linkHref: "/work/merging-brands-afriplay",
    href: "/work#brand",
  },
  {
    title: "content",
    subtitle: "we create, produce and localise content with connecting power.",
    image: "/images/Competence/Content.jpg",
    link: "See what we did for OMAC",
    linkHref: "/work/creating-engaging-content-omac",
    href: "/work#content",
  },
  {
    title: "media",
    subtitle:
      "we deliver impactful experiences right where and when it matters.",
    image: "/images/Competence/Media.jpg",
    link: "See what we did for USAID",
    linkHref: "/work/marketing-for-government-agencies-usaid",
    href: "/work#media",
  },
  {
    title: "strategy",
    subtitle:
      "we develop communication strategies then turn them into reality.",
    image: "/images/Competence/Strategy.jpg",
    link: "See what we did for E-chir",
    linkHref: "/work/marketing-for-healthcare-services-e-chir",
    href: "/work#strategy",
  },
];

export default function Home() {
  console.log("sprkl.co");
  let secondSection = useRef(null);
  let description = useRef(null);
  let linkRef = useRef(null);
  let sliderInner = useRef(null);
  let sliderIcon = useRef(null);
  let slider = useRef(null);
  let carousel = useRef(null);
  let container = useRef(null);
  let section = useRef(null);

  const [carouselBackground, setCarouselBackground] = useState();
  const [link, setLink] = useState();
  const [linkHref, setLinkHref] = useState("#");
  const [subtitle, setSubtitle] = useState();
  const [touchStart, setTouchStart] = useState(0);
  const [scrollStart, setScrollStart] = useState(0);
  const [touchEnd, setTouchEnd] = useState(0);

  const sliderTl = useRef();

  useEffect(() => {
    // timeline for slider
    sliderTl.current = gsap
      .timeline()
      .call(() => setCarouselBackground("brand"))
      .call(() => slider.current.moveRight("renderFalse"))
      .fromTo(container.current, { opacity: 0 }, { opacity: 1, duration: 0.5 })
      .fromTo(
        sliderIcon.current,
        {
          autoAlpha: 0,
        },
        {
          duration: 0.5,
          autoAlpha: 1,
          ease: Power2.easeIn,
        },
        0
      )
      .to(sliderIcon.current, {
        y: -30,
        duration: 1 / 4,
        ease: Power2.easeOut,
      })
      .to(sliderIcon.current, {
        y: 0,
        duration: 1 / 2,
        ease: Bounce.easeOut,
      })
      .fromTo(
        description.current,
        {
          autoAlpha: 0,
        },
        {
          duration: 0.5,
          autoAlpha: 1,
          ease: Power2.easeIn,
        },
        "<"
      )
      .fromTo(
        linkRef.current,
        {
          autoAlpha: 0,
        },
        {
          duration: 0.5,
          autoAlpha: 1,
          ease: Power2.easeIn,
        },
        "<"
      )
      .pause();

    // start slider animation
    ScrollTrigger.create({
      id: "carousel",
      trigger: sliderInner.current,
      animation: sliderTl.current.play(),
      start: "top center+=300",
      end: "bottom bottom",
    });

    // carousel animation
    ScrollTrigger.create({
      // markers: true,
      id: "carouselContent",
      trigger: section.current,
      onEnter: carousel.current.moveRight,
      once: true,
      start: "top top",
      end: "bottom top",
    });

    return () =>
      ScrollTrigger.getAll().forEach((t) => {
        if (t.vars.id !== "menu1") t.kill();
      });
  }, []);

  // edit slider background based on current active slide
  function editImage(sliderImage) {
    setCarouselBackground(sliderImage);
  }
  // edit slider content
  function editCarousel(link, linkHref, subtitle) {
    setLink(link);
    setLinkHref(linkHref);
    setSubtitle(subtitle);
  }
  // animating slider with touch events
  function handleTouchStart(e) {
    setTouchStart(e.targetTouches[0].clientX);
    setScrollStart(window.scrollY);
  }

  function handleTouchMove(e) {
    setTouchEnd(e.targetTouches[0].clientX);
    if (
      touchStart - e.targetTouches[0].clientX > 10 &&
      e.targetTouches[0].clientX !== 0 &&
      window.scrollY === scrollStart
    ) {
      slider.current.moveRight("notFirstRender");
    }

    if (
      touchStart - e.targetTouches[0].clientX < -10 &&
      e.targetTouches[0].clientX !== 0 &&
      window.scrollY === scrollStart
    ) {
      slider.current.moveLeft();
    }
  }

  function handleTouchEnd() {
    setTouchStart(0);
  }

  return (
    <>
      <HeroAnimation />
      <div
        ref={secondSection}
        className="bg-primary relative overflow-hidden z-10"
      >
        <Section
          ref={section}
          extraClasses="mt-20 lg:mt-36"
          extraChildrenClasses="mt-5 mb-16 lg:mb-24"
          coloredHeading="focus"
          whiteHeading="sectors"
        >
          {" "}
          Proximity to select verticals and their people allows us to identify
          key trends and themes that drive future growth and produce more
          meaningful work.
        </Section>
        <Carousel
          ref={carousel}
          classes=""
          name="home"
          items={carouselItems}
          active={1}
        />
        <Section
          extraClasses="mt-20 lg:mt-36"
          extraChildrenClasses="mt-5 mb-16 lg:mb-24"
          coloredHeading="competence"
          whiteHeading="units"
        >
          {" "}
          Our capabilities are in tune with today’s attention span, the vitality
          of omni-channel brand interactions, and marketers’ prevalent drive for
          better performance and richer knowledge.
        </Section>
        <div
          onTouchEnd={handleTouchEnd}
          onTouchStart={handleTouchStart}
          onTouchMove={handleTouchMove}
          className="relative h-400 lg:h-auto bg-center -bottom-1/2 bg-contain bg-no-repeat w-screen mx-auto"
        >
          <TransitionGroup>
            {sliderItems.map((item, index) => {
              if (item.title === carouselBackground) {
                return (
                  <CSSTransition
                    classNames="item"
                    component={null}
                    key={index}
                    timeout={{ enter: 1000, exit: 500 }}
                  >
                    <>
                      <img
                        height={800}
                        width={1920}
                        src={item.image}
                        className="absolute top-0 left-0 right-0 bottom-0 m-auto mt-0 mb-0 h-400 lg:h-auto lg:object-none object-cover"
                      />
                    </>
                  </CSSTransition>
                );
              }
            })}
          </TransitionGroup>
          <img
            height={800}
            width={1920}
            src="/images/Competence/Content.jpg"
            className="invisible h-400 lg:h-auto lg:object-none object-cover"
          />
          <div className="absolute top-0 left-0 right-0 bottom-0 m-auto w-screen h-3/5 md:h-3/4 lg:h-3/4 xl:h-1/2">
            <div ref={container}>
              <Carousel
                ref={slider}
                animateCarousel={editCarousel}
                animateImage={editImage}
                classes="slider"
                name="homeSlider"
                items={sliderItems}
                active={1}
              />
            </div>
            <div
              ref={sliderInner}
              className="mx-auto absolute left-0 right-0 bottom-2 md:bottom-0 w-full h-3/4 md:w-11/12 lg:w-8/12 xl:w-6/12 2xl:w-5/12 z-10"
            >
              <SliderIcon ref={sliderIcon} />
              <p
                ref={description}
                className=" font-futuramaxi mx-auto w-full my-5 px-5 lg:px-0 lg:my-10 text-xl font-light md:text-2xl lg:text-3xl text-white text-center"
              >
                {subtitle}
              </p>
              <div ref={linkRef}>
                <AnimatedLink
                  href={linkHref}
                  extraClasses="w-full lg:w-3/4 mt-10 lg:mt-0 mx-auto justify-center lg:text-xl"
                  extraContainerClass="lg:max-w-7xl w-5/6 mx-auto"
                >
                  {link}
                </AnimatedLink>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
