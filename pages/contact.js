import Section from "../components/section";
import BoxContact from "../components/boxcontact";
import LinkedIn from "../components/svg/linkedin";
import Youtube from "../components/svg/youtube";
import Head from "next/head";
import { useEffect, useRef, useState } from "react";
import gsap from "gsap/dist/gsap";

const contacts = [
  {
    domain: ".ae",
    firstAddress: "DTEC, Dubai",
    secondAddress: "Silicon Oasis, UAE",
    email: "ae@sprkl.co",
    phone: " +971522446929",
    whatsapp: "971522446929",
  },
  {
    domain: ".lb",
    firstAddress: "Martakla, Hazmieh,",
    secondAddress: "Beirut, Lebanon",
    email: "lb@sprkl.co",
    phone: "+9615451319",
    whatsapp: "9613008216",
  },
  {
    domain: ".cy",
    firstAddress: "Pavlou Valdaseridi,",
    secondAddress: "Larnaca, Cyprus",
    email: "cy@sprkl.co",
    phone: "+35795759256",
    whatsapp: "+35795759256",
  },
  {
    domain: ".nl",
    firstAddress: "Kerkstraat 91 A,",
    secondAddress: "Hilversum, Netherlands",
    email: "nl@sprkl.co",
    phone: "+31645941444",
    whatsapp: "31645941444",
  },
  {
    domain: ".ng",
    firstAddress: "Lekki, Phase I,",
    secondAddress: "Lagos, Nigeria",
    email: "ng@sprkl.co",
    phone: "+2349090551095",
    whatsapp: "2349090551095",
  },
];

export default function contact() {
  let linIn = useRef(null);
  let youtube = useRef(null);
  let icon1 = useRef(null);
  let icon2 = useRef(null);
  const [animation1, setAnimation1] = useState();
  const [animation2, setAnimation2] = useState();

  useEffect(() => {
    setAnimation1(
      gsap.to(icon1.current, { borderColor: "#FF00AA", duration: 0.3 }).pause()
    );
    setAnimation2(
      gsap.to(icon2.current, { borderColor: "#FF00AA", duration: 0.3 }).pause()
    );
  }, []);

  return (
    <>
      <Head>
        <title> Contact</title>
      </Head>
      <Section
        extraClasses="mt-44 lg:mt-56 "
        extraChildrenClasses="mt-5 mb-9 lg:w-9/12 lg:max-w-full"
        coloredHeading="contact"
      >
        How can we help you grow? Get in touch with any of our offices and let’s
        get to know each other.
      </Section>
      {/* <img
        width={800}
        height={1920}
        src="/images/Banners/Focus/Healthcare.jpg"
        alt="contact image"
        className="w-screen mt-24 object-cover h-400 lg:h-auto lg:object-none"
      /> */}
      <div className="lg:max-w-7xl w-5/6 mx-auto pt-10 md:pt-16 lg:pt-24 grid md:grid-cols-2 lg:grid-cols-3 gap-10">
        {contacts.map((item, index) => {
          return <BoxContact key={index} item={item} />;
        })}
        <div className="flex items-center justify-center lg:items-end lg:justify-end">
          <div
            ref={icon1}
            onMouseEnter={() => {
              animation1.play();
              linIn.current.animateEnter();
            }}
            onMouseLeave={() => {
              animation1.reverse();
              linIn.current.animateLeave();
            }}
            className="flex cursor-pointer items-center justify-center border-cyan rounded-lg border-4 h-28 w-28"
          >
            <a href="https://www.linkedin.com/company/sprkl" target="_blank">
              <LinkedIn ref={linIn} />
            </a>
          </div>
          <div
            ref={icon2}
            onMouseEnter={() => {
              animation2.play();
              youtube.current.animateEnter();
            }}
            onMouseLeave={() => {
              animation2.reverse();
              youtube.current.animateLeave();
            }}
            className="ml-5 flex cursor-pointer items-center justify-center border-cyan rounded-lg border-4 h-28 w-28"
          >
            <a
              href="https://www.youtube.com/channel/UC0r2cUbSoe5ycwtvpniTwtg"
              target="_blank"
            >
              <Youtube ref={youtube} />
            </a>
          </div>
        </div>
      </div>
    </>
  );
}
