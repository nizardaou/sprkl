import "tailwindcss/tailwind.css";
import "../styles/tailwind.css";
import "../styles/globals.scss";
import TransitionLayout from "../components/layout";
import ProgressBar from "@badrap/bar-of-progress";
import Router, { useRouter } from "next/router";
import { NextSeo } from "next-seo";

const progress = new ProgressBar({
  size: 5,
  color: "#00FFE6",
  delay: 0,
});
const seoFilter = [
  {
    filter: "/work/marketing-for-healthcare-services-e-chir",
    title: "E-Chir: Marketing for Healthcare Services - SPRKL ",
    image: "https://sprkl.co/images/Banners/E-CHIR.jpg",
    description:
      "Creative and cutting-edge marketing for healthcare services is our focus. Check out our work with e-chir to learn more.",
    social: {
      twitter: "",
    },
  },
  {
    filter: "/work/merging-brands-afriplay",
    title: "Afriplay: Merging Brands and Rebranding Case Study - SPRKL",
    image: "https://sprkl.co/images/Banners/AFRIPLAY.jpg",
    description:
      "Merging brands demands strategic marketing. Take a look at our rebranding case study with Afriplay to find out more.",
    social: {
      twitter: "",
    },
  },
  {
    filter: "/work/creating-engaging-content-omac",
    title: "OMAC: Creating Engaging Content to Promote Online Learning - SPRKL",
    image: "https://sprkl.co/images/Banners/OMAC.jpg",
    description:
      "We create valuable content that strengthens your brand. Take a look at how we promoted OMAC's learning platform by creating engaging content. ",
    social: {
      twitter: "",
    },
  },
  {
    filter: "/work/public-sector-marketing-strategy-eu",
    title:
      "Developing a Full Public Sector Marketing Strategy for the EU - SPRKL",
    image: "https://sprkl.co/images/Banners/EU.jpg",
    description:
      "We help diplomatic services fulfill their goals. Here is how we developed a full public marketing strategy for the European Union.",
    social: {
      twitter: "",
    },
  },
  {
    filter: "/work/NGO-marketing-nabad",
    title: "Nabad: NGO Marketing to Support Local Artists - SPRKL",
    image: "https://sprkl.co/images/Banners/NABAD.jpg",
    description:
      "NGO marketing can make all the difference. Here is how we developed a marketing strategy for Nabad to support local artists.",
    social: {
      twitter: "",
    },
  },
  {
    filter: "/work/marketing-for-government-agencies-usaid",
    title: "USAID: Marketing for Government Agencies - SPRKL",
    image: "https://sprkl.co/images/Banners/USAID.jpg",
    description:
      "Marketing for government agencies is our expertise. Have a look at how we helped USAID create awareness and achieve their goals.",
    social: {
      twitter: "",
    },
  },
  {
    filter: "/work/cloud-kitchen-case-study-yalla-shawarma",
    title: "Yalla Shawarma: A Cloud Kitchen Case Study - SPRKL",
    image: "https://sprkl.co/images/Banners/YALLA-SHAWARMA.jpg",
    description:
      "Keep your food service on the leading edge of technology. Our cloud kitchen case study with Yalla Shawarma shows how we can help.",
    social: {
      twitter: "",
    },
  },
  {
    filter: "/work/digital-transformation-in-F&B-industry",
    title: "Diego Hamburguesas: Digital Transformation in F&B Industry - SPRKL",
    image: "https://sprkl.co/images/Banners/Diego-Burgers.jpg",
    description:
      "We help businesses keep up with the digital transformation in the F&B industry. Check out our case study with Diego Hamburguesas to find out how.",
    social: {
      twitter: "",
    },
  },
  {
    filter: "/",
    title:
      "SPRKL - An Integrated Marketing Company With a Digital-First Approach",
    image: "https://sprkl.co/images/Banners/Home.jpg",
    description:
      "We are an integrated marketing company with a digital-first approach. Build and grow your brand today at digital speed with SPRKL.",
    social: {
      twitter: "",
    },
  },
];

Router.events.on("routeChangeStart", progress.start);
Router.events.on("routeChangeComplete", progress.finish);
Router.events.on("routeChangeError", progress.finish);

function MyApp({ Component, pageProps }) {
  let router = useRouter();

  return (
    <>
      {seoFilter.map((item, index) => {
        if (router.asPath === item.filter) {
          return (
            <NextSeo
              key={index}
              title={item.title}
              description={item.description}
              canonical="https://sprkl.co"
              openGraph={{
                url: `https://sprkl.co${router.asPath}`,
                title: item.title,
                type: "website",
                description: item.description,
                images: [
                  {
                    url: item.image,
                    width: 800,
                    height: 600,
                    alt: "",
                  },
                  {
                    url: item.image,
                    width: 900,
                    height: 800,
                    alt: "",
                  },
                ],
                site_name: "SPRKL",
              }}
              twitter={{
                handle: "@sprkl",
                site: "@sprkl",
                cardType: "summary_large_image",
              }}
            />
          );
        }
      })}

      <TransitionLayout>
        <Component {...pageProps} />
      </TransitionLayout>
    </>
  );
}

export default MyApp;
