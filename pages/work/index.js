import Head from "next/head";
import { useEffect, useState, useRef } from "react";
import AnimatedBox from "../../components/animatedbox";
import Section from "../../components/section";
import MultiSelect from "../../components/multiselect";
import { gsap } from "gsap/dist/gsap";
import { ScrollToPlugin } from "gsap/dist/ScrollToPlugin";
import { useRouter } from "next/router";
gsap.registerPlugin(ScrollToPlugin);

const focusItems = ["healthcare", "egaming", "public & ngo", "f&b"];

const competenceItems = [
  "strategy",
  "brand",
  "content",
  "tech",
  "media",
  "insights",
];

const workItems = [
  {
    name: "e-chir",
    focus: "healthcare",
    competence: ["strategy", "brand", "content", "tech", "insights"],
    logo: "/images/logos/E-CHIR.png",
    link: "marketing-for-healthcare-services-e-chir",
  },
  {
    name: "afriplay",
    focus: "egaming",
    competence: ["strategy", "brand"],
    logo: "/images/logos/AFRIPLAY.png",
    link: "merging-brands-afriplay",
  },
  {
    name: "omac",
    focus: "public & ngo",
    competence: ["strategy", "content"],
    logo: "/images/logos/OMAC.png",
    link: "creating-engaging-content-omac",
  },
  {
    name: "eu",
    focus: "public & ngo",
    competence: ["strategy", "content", "tech", "media", "insights"],
    logo: "/images/logos/EU.png",
    link: "public-sector-marketing-strategy-eu",
  },
  {
    name: "nabad",
    focus: "public & ngo",
    competence: ["strategy", "brand", "content", "tech", "media", "insights"],
    logo: "/images/logos/NABAD.png",
    link: "NGO-marketing-nabad",
  },
  {
    name: "usaid",
    focus: "public & ngo",
    competence: ["strategy", "content", "media", "insights"],
    logo: "/images/logos/USAID.png",
    link: "marketing-for-government-agencies-usaid",
  },
  {
    name: "yalla shawarma",
    focus: "f&b",
    competence: ["strategy", "brand", "content", "tech", "media", "insights"],
    logo: "/images/logos/YALLA SHAWARMA.png",
    link: "cloud-kitchen-case-study-yalla-shawarma",
  },
  {
    name: "diego burgers",
    focus: "f&b",
    competence: ["strategy", "brand", "content", "tech", "media", "insights"],
    logo: "/images/logos/Diego Burgers.png",
    link: "digital-transformation-in-F&B-industry",
  },
];

export default function work() {
  let router = useRouter();
  let workSection = useRef(null);
  const [focusTags, setFocusTags] = useState([]);
  const [competenceTags, setCompetenceTags] = useState([]);
  const [renderedItems, setRenderedItems] = useState([]);

  function focus(items) {
    setFocusTags(items);
  }

  function competence(items) {
    setCompetenceTags(items);
  }

  useEffect(() => {
    if (router.asPath.includes("#")) {
      gsap.to(window, {
        duration: 0.5,
        scrollTo: { y: workSection.current, start: 0 },
      });
    }
  }, []);

  useEffect(() => {
    if (focusTags.length == 0 && competenceTags.length == 0) {
      setRenderedItems(workItems);
    } else if (focusTags.length !== 0 && competenceTags.length == 0) {
      setRenderedItems(workItems.filter((e) => focusTags.includes(e.focus)));
    } else if (focusTags.length == 0 && competenceTags.length !== 0) {
      setRenderedItems(
        workItems.filter(
          (e) =>
            e.competence.filter((value) => competenceTags.includes(value))
              .length > 0
        )
      );
    } else {
      setRenderedItems(
        workItems.filter(
          (e) =>
            e.competence.filter((value) => competenceTags.includes(value))
              .length > 0 && focusTags.includes(e.focus)
        )
      );
    }
  }, [focusTags, competenceTags]);

  return (
    <>
      <Head>
        <title> Work</title>
      </Head>
      <div className="bg-primary">
        <Section
          ref={workSection}
          extraClasses="mt-44 lg:mt-56"
          extraChildrenClasses="mt-5 mb-9"
          coloredHeading="recent"
          whiteHeading="work"
          flip
        >
          We built sprkl on one philosophy: better work, better results, better
          growth - In that order. We have compiled a few cases for you to get
          closer to our way of thinking and doing things.
        </Section>
        <div className="lg:max-w-7xl w-5/6 mt-10 md:mt-16 lg:mt-20 mx-auto">
          <div className="grid lg:grid-cols-2 gap-5 lg:gap-20">
            <MultiSelect
              name="focus"
              chosenItems={focus}
              type="focus"
              droppedItems={focusItems}
            />
            <MultiSelect
              name="competence"
              chosenItems={competence}
              type="competence"
              droppedItems={competenceItems}
            />
          </div>
        </div>
        <div className="lg:max-w-7xl w-5/6 min-h-56 h-auto mx-auto mt-12 grid sm:grid-cols-2 lg:grid-cols-3 gap-9">
          {renderedItems.map((item, index) => {
            {
              return (
                <AnimatedBox
                  key={index}
                  name={item.name}
                  focus={item.focus}
                  competence={item.competence}
                  logo={item.logo}
                  link={item.link}
                />
              );
            }
          })}
        </div>
      </div>
    </>
  );
}
