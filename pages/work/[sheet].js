import { google } from "googleapis";
import HeadingText from "../../components/headingtext";
import Carousel from "../../components/carousel";
import Head from "next/head";
import HeadingImage from "../../components/headingimage";
import AnimatedLink from "../../components/animatedlink";
import { useEffect, useRef, useState } from "react";
import { gsap } from "gsap/dist/gsap";
import { ScrollToPlugin } from "gsap/dist/ScrollToPlugin";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";

gsap.registerPlugin(ScrollTrigger);
gsap.registerPlugin(ScrollToPlugin);

const echirGallery = [
  {
    asset: "/work/E-CHIR/Gallery/1.jpg",
    type: "image",
    href: "/documents/e-chir.pdf",
  },
  {
    asset: "/work/E-CHIR/Gallery/2.jpg",
    type: "image",
    href: "#",
  },
  {
    asset: "/work/E-CHIR/Gallery/3",
    type: "video",
    href: "https://e-chir.ch",
  },
  {
    asset: "/work/E-CHIR/Gallery/4",
    type: "video",
    href: "#",
  },
];

const afriplayGallery = [
  {
    asset: "/work/AFRIPLAY/Gallery/1.jpg",
    type: "image",
    href: "#",
  },
  {
    asset: "/work/AFRIPLAY/Gallery/2.jpg",
    type: "image",
    href: "#",
  },
  {
    asset: "/work/AFRIPLAY/Gallery/3.jpg",
    type: "image",
    href: "#",
  },
  {
    asset: "/work/AFRIPLAY/Gallery/4.jpg",
    type: "image",
    href: "#",
  },
  {
    asset: "/work/AFRIPLAY/Gallery/5.jpg",
    type: "image",
    href: "#",
  },
];

const diegoGallery = [
  {
    asset: "/work/Diego-Burgers/Gallery/1.jpg",
    type: "image",
    href: "#",
  },
  {
    asset: "/work/Diego-Burgers/Gallery/2.jpg",
    type: "image",
    href: "#",
  },
  {
    asset: "/work/Diego-Burgers/Gallery/3.jpg",
    type: "image",
    href: "#",
  },
  {
    asset: "/work/Diego-Burgers/Gallery/4.jpg",
    type: "image",
    href: "#",
  },
];

const yallaGallery = [
  {
    asset: "/work/YALLA-SHAWARMA/Gallery/1.jpg",
    type: "image",
    href: "#",
  },
  {
    asset: "/work/YALLA-SHAWARMA/Gallery/2.jpg",
    type: "image",
    href: "#",
  },
  {
    asset: "/work/YALLA-SHAWARMA/Gallery/3.jpg",
    type: "image",
    href: "#",
  },
  {
    asset: "/work/YALLA-SHAWARMA/Gallery/4",
    type: "video",
    href: "https://youtu.be/sROzdJmxWBc",
  },
  {
    asset: "/work/YALLA-SHAWARMA/Gallery/5",
    type: "video",
    href: "https://youtu.be/cjQnw0vBKo0",
  },
  {
    asset: "/work/YALLA-SHAWARMA/Gallery/6",
    type: "video",
    href: "https://youtu.be/XCEtaBqLTa4",
  },
];

const euGallery = [
  {
    asset: "/work/EU/Gallery/1.jpg",
    type: "image",
    href: "#",
  },
  {
    asset: "/work/EU/Gallery/2.jpg",
    type: "image",
    href: "#",
  },
  {
    asset: "/work/EU/Gallery/3.jpg",
    type: "image",
    href: "#",
  },
  {
    asset: "/work/EU/Gallery/4",
    type: "video",
    href: "https://youtu.be/CegrCtuycKY",
  },
  {
    asset: "/work/EU/Gallery/5",
    type: "video",
    href: "https://youtu.be/h8-Wq_-jbFQ",
  },
  {
    asset: "/work/EU/Gallery/6",
    type: "video",
    href: "https://youtu.be/TaV0Mv2irdI",
  },
];

const usaidGallery = [
  {
    asset: "/work/USAID/Gallery/1",
    type: "video",
    href: "https://youtu.be/VZhKswhsYhA",
  },
  {
    asset: "/work/USAID/Gallery/2",
    type: "video",
    href: "https://youtu.be/KdgXrOFDmGU",
  },
  {
    asset: "/work/USAID/Gallery/3",
    type: "video",
    href: "#",
  },
  {
    asset: "/work/USAID/Gallery/4",
    type: "video",
    href: "#",
  },
];

const nabadGallery = [
  {
    asset: "/work/NABAD/Gallery/1",
    type: "video",
    href: "#",
  },
  {
    asset: "/work/NABAD/Gallery/2.jpg",
    type: "image",
    href: "#",
  },
  {
    asset: "/work/NABAD/Gallery/3",
    type: "video",
    href: "https://youtu.be/iSEExL0zk-M",
  },
  {
    asset: "/work/NABAD/Gallery/4",
    type: "video",
    href: "#",
  },
];

const omacGallery = [
  {
    asset: "/work/OMAC/Gallery/1",
    type: "video",
    href: "https://youtu.be/Qlh2AspVssg",
  },
  {
    asset: "/work/OMAC/Gallery/2",
    type: "video",
    href: "https://youtu.be/gWjClQ6YDHM",
  },
  {
    asset: "/work/OMAC/Gallery/3",
    type: "video",
    href: "https://youtu.be/feD6bcjwJRo",
  },
  {
    asset: "/work/OMAC/Gallery/4",
    type: "video",
    href: "https://youtu.be/uc7a5wbG6bA",
  },
];

// This function gets called at build time
export async function getStaticPaths() {
  const projects = [
    "marketing-for-healthcare-services-e-chir",
    "merging-brands-afriplay",
    "creating-engaging-content-omac",
    "public-sector-marketing-strategy-eu",
    "cloud-kitchen-case-study-yalla-shawarma",
    "NGO-marketing-nabad",
    "marketing-for-government-agencies-usaid",
    "digital-transformation-in-F&B-industry",
  ];
  // console.log(projects);

  // Get the paths we want to pre-render based on posts
  const paths = projects.map((project, index) => ({
    params: { sheet: project },
  }));

  // We'll pre-render only these paths at build time.
  // { fallback: false } means other routes should 404.
  return { paths, fallback: false };
}

export async function getStaticProps({ params }) {
  // Auth
  const auth = await google.auth.getClient({
    scopes: ["https://www.googleapis.com/auth/spreadsheets.readonly"],
  });

  const sheets = google.sheets({ version: "v4", auth });

  // params
  const { sheet } = params;
  const range = `${sheet}!A2:J2`;

  const response = await sheets.spreadsheets.values.get({
    spreadsheetId: process.env.SHEET_ID,
    range,
  });

  // Result
  const [
    name,
    imageText,
    assets,
    focus,
    competence,
    market,
    context,
    pov,
    solution,
    outcome,
  ] = response.data.values[0];

  return {
    props: {
      name,
      imageText,
      assets,
      focus,
      competence,
      market,
      context,
      pov,
      solution,
      outcome,
    },
    revalidate: 10,
  };
}

export default function Project({
  name,
  imageText,
  assets,
  focus,
  competence,
  market,
  context,
  pov,
  solution,
  outcome,
}) {
  let video = useRef();
  let carousel = useRef(null);
  let container = useRef(null);
  let banner = useRef();
  const [videoPlayed, setPlayVideo] = useState(false);
  const [mobile, setMobile] = useState(false);

  useEffect(() => {
    if (video.current) {
      if (videoPlayed) {
        video.current.play();
      } else if (!videoPlayed) {
        video.current.pause();
      }
    }
  }, [videoPlayed]);

  useEffect(() => {
    // carousel animation. Added timeout to wait for page loading
    setTimeout(
      () =>
        ScrollTrigger.create({
          // markers: true,
          id: "carouselContent",
          trigger: container.current,
          onEnter: carousel.current.moveRight,
          once: true,
          start: "bottom top",
          end: "+=100%",
        }),
      50
    );

    // state to add controls to video on mobile screens
    if (window.innerWidth < 768) {
      setMobile(true);
    }
    gsap.to(window, {
      duration: 0.5,
      scrollTo: { y: banner.current, start: 0 },
    });
    return () => {
      setPlayVideo();
      ScrollTrigger.getAll().forEach((t) => {
        if (t.vars.id !== "menu1") t.kill();
      });
    };
  }, []);
  return (
    <>
      {/* <Head>
        <title>{title}</title>
      </Head> */}

      <div ref={banner}>
        <HeadingImage
          id={name}
          heading={imageText}
          image={`/images/Banners/${name}.jpg`}
        />
      </div>
      <div className="bg-primary mx-auto lg:max-w-7xl w-5/6">
        <span className="inline-block">
          <img
            src={`/work/Logos/${name}.png`}
            width={400}
            height={300}
            className="max-w-full my-12 w-1/2"
          />
        </span>
        <p className="text-cyan text-xl md:text-2xl lg:text-3xl">focus:</p>
        <p className="text-white text-xl md:text-2xl lg:text-3xl">{focus}</p>
        <p className="text-cyan mt-5 text-xl md:text-2xl lg:text-3xl">
          competence:
        </p>
        <p className="text-white text-xl md:text-2xl lg:text-3xl">
          {competence}
        </p>
        {assets != "" && (
          <p className="text-cyan mt-5 text-xl md:text-2xl lg:text-3xl">
            assets:
          </p>
        )}
        {assets != "" &&
          assets.split(",").map((link, index) => {
            return (
              <AnimatedLink
                key={index}
                extraContainerClass="mt-1"
                extraClasses="text-xl md:text-2xl lg:text-3xl"
                type="carousel"
                href={`https://${link}`}
                blank
              >
                {link}
              </AnimatedLink>
            );
          })}

        <p className="text-cyan mt-5 text-xl md:text-2xl lg:text-3xl">
          market:
        </p>
        <p className="text-white text-xl md:text-2xl lg:text-3xl">{market}</p>
      </div>
      <HeadingText heading="context / opportunity">{context}</HeadingText>
      {name !== "AFRIPLAY" && name !== "YALLA-SHAWARMA" && (
        <video
          controls={mobile ? true : false}
          ref={video}
          preload="auto"
          className={`w-screen mt-16 lg:mt-24 ${
            videoPlayed ? "pause" : "play"
          }`}
          onClick={() => {
            setPlayVideo(!videoPlayed);
          }}
        >
          <source src={`/work/${name}/Video/1.mp4`} type="video/mp4" />
          {/* <source src={videoSourceMp4} type="video/mp4" /> */}
          Your browser does not support the video tag.
        </video>
      )}

      <HeadingText heading="point of view">{pov}</HeadingText>
      <div ref={container}>
        <HeadingText heading="solutions">{solution}</HeadingText>
      </div>

      {name === "E-CHIR" && (
        <Carousel
          ref={carousel}
          name="projects"
          classes=""
          items={echirGallery}
          active={1}
        />
      )}
      {name === "AFRIPLAY" && (
        <Carousel
          ref={carousel}
          name="projects"
          classes=""
          items={afriplayGallery}
          active={1}
        />
      )}
      {name === "USAID" && (
        <Carousel
          ref={carousel}
          name="projects"
          classes=""
          items={usaidGallery}
          active={1}
        />
      )}
      {name === "YALLA-SHAWARMA" && (
        <Carousel
          ref={carousel}
          name="projects"
          classes=""
          items={yallaGallery}
          active={1}
        />
      )}
      {name === "NABAD" && (
        <Carousel
          ref={carousel}
          name="projects"
          classes=""
          items={nabadGallery}
          active={1}
        />
      )}
      {name === "OMAC" && (
        <Carousel
          ref={carousel}
          name="projects"
          classes=""
          items={omacGallery}
          active={1}
        />
      )}
      {name === "EU" && (
        <Carousel
          ref={carousel}
          name="projects"
          classes=""
          items={euGallery}
          active={1}
        />
      )}
      {name === "Diego-Burgers" && (
        <Carousel
          ref={carousel}
          name="projects"
          classes=""
          items={diegoGallery}
          active={1}
        />
      )}

      <HeadingText heading="outcome">{outcome}</HeadingText>
    </>
  );
}
