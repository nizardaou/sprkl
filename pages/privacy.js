import Head from "next/head";
import Section from "../components/section";

export default function Privacy() {
  return (
    <>
      <Head>
        <title>Terms & Privacy</title>
      </Head>
      <Section
        extraClasses="mt-44 lg:mt-56"
        extraChildrenClasses="mt-5 mb-9 lg:w-9/12 lg:max-w-full"
        coloredHeading="Terms of"
        whiteHeading="Service"
      >
        <ol className="text-lg md:text-xl lg:text-2xl list-decimal list-inside mt-5 mb-10">
          <li className="my-5 font-bold">Terms</li>
          <p className="text-lg md:text-xl lg:text-2xl " className="mt-5 mb-10">
            By accessing the website at http://www.sprkl.co, you are agreeing to
            be bound by these terms of service, all applicable laws and
            regulations, and agree that you are responsible for compliance with
            any applicable local laws. If you do not agree with any of these
            terms, you are prohibited from using or accessing this site. The
            materials contained in this website are protected by applicable
            copyright and trademark law.
          </p>
          <ol className="text-lg md:text-xl lg:text-2xl list-decimal list-inside pl-10 mt-5 mb-10">
            <li className="mt-5">
              Minors under the age of 18 shall are prohibited to register as a
              User of this website and are not allowed to transact or use the
              website.
            </li>
            <li className="mt-5">
              If you make a payment for our products or services on our website,
              the details you are asked to submit will be provided directly to
              our payment provider via a secured connection.
            </li>
            <li className="mt-5">
              The cardholder must retain a copy of transaction records and
              Merchant policies and rules.
            </li>
            <li className="mt-5">
              SPRKL – FZCO will NOT deal or provide any services or products to
              any of OFAC (Office of Foreign Assets Control) sanctions countries
              in accordance with the law of UAE
            </li>
            <li className="mt-5">
              We accept payments online using Visa and MasterCard credit/debit
              card in AED and USD
            </li>
          </ol>
          <li className="my-5 font-bold">Use License</li>
          <ol className="text-lg md:text-xl lg:text-2xl list-decimal list-inside mt-5 mb-10 pl-10">
            <li className="mt-5">
              Permission is granted to temporarily download one copy of the
              materials (information or software) on SPRKL – FZCO’s website for
              personal, non-commercial transitory viewing only. This is the
              grant of a license, not a transfer of title, and under this
              license you may not:
            </li>
            <ol className="text-lg md:text-xl lg:text-2xl list-decimal list-inside mt-5 mb-10 pl-10">
              <li className="mt-5">modify or copy the materials;</li>
              <li className="mt-5">
                use the materials for any commercial purpose, or for any public
                display (commercial or non-commercial);
              </li>
              <li className="mt-5">
                attempt to decompile or reverse engineer any software contained
                on SPRKL – FZCO’s website;
              </li>
              <li className="mt-5">
                remove any copyright or other proprietary notations from the
                materials; or
              </li>
              <li className="mt-5">
                transfer the materials to another person or “mirror” the
                materials on any other server.
              </li>
            </ol>
            <li className="mt-5">
              This license shall automatically terminate if you violate any of
              these restrictions and may be terminated by SPRKL – FZCO at any
              time. Upon terminating your viewing of these materials or upon the
              termination of this license, you must destroy any downloaded
              materials in your possession whether in electronic or printed
              format.
            </li>
          </ol>
          <li className="my-5 font-bold">Disclaimer</li>
          <ol className="text-lg md:text-xl lg:text-2xl list-decimal list-inside mt-5 mb-10 pl-10">
            <li className="mt-5">
              The materials on SPRKL – FZCO’s website are provided on an ‘as is’
              basis. SPRKL – FZCO makes no warranties, expressed or implied, and
              hereby disclaims and negates all other warranties including,
              without limitation, implied warranties or conditions of
              merchantability, fitness for a particular purpose, or
              non-infringement of intellectual property or other violation of
              rights.
            </li>
            <li className="mt-5">
              Further, SPRKL – FZCO does not warrant or make any representations
              concerning the accuracy, likely results, or reliability of the use
              of the materials on its website or otherwise relating to such
              materials or on any sites linked to this site.
            </li>
          </ol>
          <li className="my-5 font-bold">Limitations</li>
          <p className="text-lg md:text-xl lg:text-2xl ">
            In no event shall SPRKL – FZCO or its suppliers be liable for any
            damages (including, without limitation, damages for loss of data or
            profit, or due to business interruption) arising out of the use or
            inability to use the materials on SPRKL – FZCO’s website, even if
            SPRKL – FZCO or a SPRKL – FZCO authorized representative has been
            notified orally or in writing of the possibility of such damage.
            Because some jurisdictions do not allow limitations on implied
            warranties, or limitations of liability for consequential or
            incidental damages, these limitations may not apply to you.
          </p>
          <li className="my-5 font-bold">Accuracy of materials</li>
          <p className="text-lg md:text-xl lg:text-2xl ">
            {" "}
            The materials appearing on SPRKL – FZCO’s website could include
            technical, typographical, or photographic errors. SPRKL – FZCO does
            not warrant that any of the materials on its website are accurate,
            complete or current. SPRKL – FZCO may make changes to the materials
            contained on its website at any time without notice. However SPRKL –
            FZCO does not make any commitment to update the materials.
          </p>
          <li className="my-5 font-bold">Links</li>
          <p className="text-lg md:text-xl lg:text-2xl ">
            SPRKL – FZCO has not reviewed all of the sites linked to its website
            and is not responsible for the contents of any such linked site. The
            inclusion of any link does not imply endorsement by SPRKL – FZCO of
            the site. Use of any such linked website is at the user’s own risk.
          </p>
          <li className="my-5 font-bold">Modifications</li>
          <p className="text-lg md:text-xl lg:text-2xl ">
            SPRKL – FZCO may revise these terms of service for its website at
            any time without notice. By using this website you are agreeing to
            be bound by the then current version of these terms of service.
          </p>
          <li className="my-5 font-bold">Governing Law</li>
          <ol className="text-lg md:text-xl lg:text-2xl list-decimal list-inside mt-5 mb-10 pl-10">
            <li className="mt-5">
              Any dispute or claim arising out of or in connection with this
              website shall be governed and construed in accordance with the
              laws of UAE.
            </li>
            <li className="mt-5">
              United Arab of Emirates is our country of domicile.
            </li>
          </ol>
          <li className="my-5"> Refund and Cancelation</li>
          <p className="text-lg md:text-xl lg:text-2xl">
            Refunds will be done only through the Original Mode of Payment.
          </p>
        </ol>
      </Section>
      <Section
        extraClasses="mt-20 lg:mt-32"
        extraChildrenClasses="mt-5 mb-9 lg:w-9/12 lg:max-w-full"
        coloredHeading="Privacy"
        whiteHeading="Policy"
      >
        <p className="text-lg md:text-xl lg:text-2xl ">
          Your privacy is important to us.
        </p>
        <p className="text-lg md:text-xl lg:text-2xl mt-5 mb-10">
          {" "}
          It is SPRKL – FZCO’s policy to respect your privacy regarding any
          information we may collect while operating our website. Accordingly,
          we have developed this privacy policy in order for you to understand
          how we collect, use, communicate, disclose and otherwise make use of
          personal information. We have outlined our privacy policy below.
        </p>
        <ul className="text-lg md:text-xl lg:text-2xl list-disc mt-5 mb-10 pl-10">
          <li className="mt-5">
            We will collect personal information by lawful and fair means and,
            where appropriate, with the knowledge or consent of the individual
            concerned.
          </li>
          <li className="mt-5">
            Before or at the time of collecting personal information, we will
            identify the purposes for which information is being collected.
          </li>
          <li className="mt-5">
            We will collect and use personal information solely for fulfilling
            those purposes specified by us and for other ancillary purposes,
            unless we obtain the consent of the individual concerned or as
            required by law.
          </li>
          <li className="mt-5">
            Personal data should be relevant to the purposes for which it is to
            be used, and, to the extent necessary for those purposes, should be
            accurate, complete, and up-to-date.
          </li>
          <li className="mt-5">
            We will protect personal information by using reasonable security
            safeguards against loss or theft, as well as unauthorized access,
            disclosure, copying, use or modification.
          </li>
          <li className="mt-5">
            We will make readily available to customers information about our
            policies and practices relating to the management of personal
            information.
          </li>
          <li className="mt-5">
            We will only retain personal information for as long as necessary
            for the fulfilment of those purposes.
          </li>
          <li className="mt-5">
            All credit/debit cards details and personally identifiable
            information will NOT be stored, sold, shared, rented or leased to
            any third parties.
          </li>
          <li className="mt-5">
            The Website Policies and Terms & Conditions may be changed or
            updated occasionally to meet the requirements and standards.
            Therefore the Customers’ are encouraged to frequently visit these
            sections in order to be updated about the changes on the website.
            Modifications will be effective on the day they are posted.
          </li>
          <li className="mt-5">
            Some of the advertisements you see on the Site are selected and
            delivered by third parties, such as ad networks, advertising
            agencies, advertisers, and audience segment providers. These third
            parties may collect information about you and your online
            activities, either on the Site or on other websites, through
            cookies, web beacons, and other technologies in an effort to
            understand your interests and deliver to you advertisements that are
            tailored to your interests. Please remember that we do not have
            access to, or control over, the information these third parties may
            collect. The information practices of these third parties are not
            covered by this privacy policy.
          </li>
        </ul>
        <p className="text-lg md:text-xl lg:text-2xl ">
          We are committed to conducting our business in accordance with these
          principles in order to ensure that the confidentiality of personal
          information is protected and maintained. SPRKL – FZCO may change this
          privacy policy from time to time at SPRKL – FZCO’s sole discretion.
        </p>
      </Section>
    </>
  );
}
