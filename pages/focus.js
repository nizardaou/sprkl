import Section from "../components/section";
import HeadingImage from "../components/headingimage";
import HeadingText from "../components/headingtext";
import AnimatedBox from "../components/animatedbox";
import AnimatedLink from "../components/animatedlink";
import Head from "next/head";
import { useEffect, useRef } from "react";
import { gsap } from "gsap/dist/gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import { ScrollToPlugin } from "gsap/dist/ScrollToPlugin";
import { useRouter } from "next/router";
gsap.registerPlugin(ScrollTrigger);
gsap.registerPlugin(ScrollToPlugin);

export default function focus() {
  let router = useRouter();
  let focus = useRef(null);
  let secondSection = useRef(null);
  let thirdSection = useRef(null);
  let secondMobile = useRef(null);
  let thirdMobile = useRef(null);
  let fourthSection = useRef(null);
  let killed;
  let food = useRef(null);
  let egaming = useRef(null);
  let healthcare = useRef(null);
  let ngo = useRef(null);

  useEffect(() => {
    if (router.asPath === "/focus#f&b") {
      gsap.to(window, {
        duration: 0.5,
        scrollTo: { y: food.current, start: 0 },
      });
    } else if (router.asPath === "/focus#healthcare") {
      gsap.to(window, {
        duration: 0.5,
        scrollTo: { y: healthcare.current, start: 0 },
      });
    } else if (router.asPath === "/focus#egaming") {
      gsap.to(window, {
        duration: 0.5,
        scrollTo: { y: egaming.current },
      });
    } else if (router.asPath === "/focus#public&ngo") {
      gsap.to(window, {
        duration: 0.5,
        scrollTo: { y: ngo.current },
      });
    }
  }, []);

  useEffect(() => {
    ScrollTrigger.matchMedia({
      "(min-width:1024px)": () => {
        if (!killed) {
          ScrollTrigger.create({
            id: "sectors",
            trigger: focus.current,
            start: 0,
            end: "+=100%",
            pin: true,
            pinSpacing: false,
          });
          ScrollTrigger.create({
            id: "second",
            trigger: secondSection.current,
            end: "+=100%",
            pin: true,
            pinSpacing: false,
          });
          ScrollTrigger.create({
            id: "third",
            trigger: thirdSection.current,
            end: "+=100%",
            pin: true,
            pinSpacing: false,
          });
          ScrollTrigger.create({
            id: "fourth",
            trigger: fourthSection.current,
            end: "+=100%",
            pin: true,
            pinSpacing: false,
          });
        }
      },

      "(min-width: 600px) and (max-width: 1023px)": () => {
        if (!killed) {
          ScrollTrigger.create({
            anticipatePin: true,
            id: "sectorsSmall",
            trigger: focus.current,
            start: 0,
            end: "+=200%",
            pin: true,
            pinSpacing: false,
          });

          ScrollTrigger.create({
            anticipatePin: true,
            id: "secondSmall",
            trigger: secondMobile.current,
            start: "top top",
            end: "+=150%",
            pin: true,
            anticipatePin: true,
            pinSpacing: false,
          });
          ScrollTrigger.create({
            anticipatePin: true,
            id: "thirdSmall",
            trigger: thirdMobile.current,
            start: "top top",
            end: "+=150%",
            pin: true,
            anticipatePin: true,
            pinSpacing: false,
          });
          ScrollTrigger.create({
            anticipatePin: true,
            id: "fourthSmall",
            trigger: fourthSection.current,
            start: "top top",
            end: "+=150%",
            pin: true,
            pinSpacing: false,
          });
        }
      },

      "(max-width: 599px)": () => {
        if (!killed) {
          ScrollTrigger.create({
            anticipatePin: true,
            id: "sectorsSmall",
            trigger: focus.current,
            start: 0,
            end: "bottom top",
            pin: true,
            pinSpacing: false,
          });

          ScrollTrigger.create({
            anticipatePin: true,
            id: "secondSmall",
            trigger: secondMobile.current,
            start: "bottom bottom",
            end: "+=200 10%",
            pin: true,
            pinSpacing: false,
          });
          ScrollTrigger.create({
            anticipatePin: true,
            id: "thirdSmall",
            trigger: thirdMobile.current,
            start: "bottom bottom",
            end: "+=200 10%",
            pin: true,
            pinSpacing: false,
          });
          ScrollTrigger.create({
            anticipatePin: true,
            id: "fourthSmall",
            trigger: fourthSection.current,
            start: "bottom bottom",
            end: "+=200 10%",
            pin: true,
            pinSpacing: false,
          });
        }
      },
    });

    return () =>
      ScrollTrigger.getAll().forEach((t) => {
        if (t.vars.id != "menu1") {
          t.kill();
          killed = true;
        }
      });
  });

  return (
    <>
      <Head>
        <title>Focus</title>
      </Head>
      <div className="bg-primary relative">
        <div className="relative overflow-hidden max-w-full h-full bg-primary">
          <div className="flex justify-center w-full h-full mx-auto">
            <div className="lg:max-w-full lg:w-full">
              <Section
                ref={focus}
                extraClasses="mt-44 lg:mt-56"
                extraChildrenClasses="mt-5 mb-9 lg:w-9/12 lg:max-w-full"
                coloredHeading="focus"
                whiteHeading="sectors"
              >
                We believe that intimate knowledge of an industry is a paramount
                condition for adding meaningful value to growing companies.
                We’ve built track records and gained deep knowledge in our focus
                sectors:
              </Section>

              <HeadingImage
                ref={healthcare}
                heading="healthcare"
                image="/images/Banners/Focus/Healthcare.jpg"
              />
              <div ref={secondMobile}>
                <HeadingText heading="Enabling the next normal">
                  We are experts at building meaningful stories and articulating
                  the positive ways that your healthcare products or services
                  can change people’s lives.
                </HeadingText>
                <div
                  ref={secondSection}
                  className="bg-primary w-screen relative mx-auto"
                >
                  <div className="lg:max-w-7xl w-5/6 mx-auto pt-10 md:pt-16 lg:pt-24 grid sm:grid-cols-2 lg:grid-cols-3 gap-9 bg-primary">
                    <AnimatedBox
                      name="e-chir"
                      focus="healthcare"
                      competence={["strategy", "brand", "content", "tech"]}
                      logo="/images/logos/E-CHIR.png"
                      link="marketing-for-healthcare-services-e-chir"
                    />
                  </div>
                  <AnimatedLink
                    href="/work#healthcare"
                    extraClasses="w-96 text-xl"
                    extraContainerClass="lg:max-w-7xl w-5/6 mx-auto pt-10 md:pt-16 lg:pt-24 pb-20 lg:pb-0 bg-primary relative"
                  >
                    See all healthcare work
                  </AnimatedLink>
                </div>
              </div>

              <HeadingImage
                ref={egaming}
                heading="eGaming"
                image="/images/Banners/Focus/gaming.jpg"
              />
              <div ref={thirdMobile}>
                <HeadingText heading="True 360° support">
                  When it comes to eGaming, Brand is not only king, it is
                  everything. Over the years, we have developed a particularly
                  ubiquitous offering at the service of egaming companies from
                  the realms of brand development to the setup of analytical
                  tools and business intelligence systems.
                </HeadingText>
                <div ref={thirdSection}>
                  <div className="lg:max-w-7xl w-5/6 mx-auto pt-10 md:pt-16 lg:pt-24 grid sm:grid-cols-2 lg:grid-cols-3 gap-9 bg-primary">
                    <AnimatedBox
                      name="afriplay"
                      focus="egaming"
                      competence={["strategy", "brand"]}
                      logo="/images/logos/AFRIPLAY.png"
                      link="merging-brands-afriplay"
                    />
                  </div>
                  <AnimatedLink
                    href="/work#egaming"
                    extraClasses="w-96 text-xl"
                    extraContainerClass="lg:max-w-7xl w-5/6 mx-auto pt-10 md:pt-16 lg:pt-24 pb-20 lg:pb-0 bg-primary relative"
                  >
                    See all eGaming work
                  </AnimatedLink>
                </div>
              </div>
              <HeadingImage
                ref={ngo}
                heading="public & ngo"
                image="/images/Banners/Focus/public&ngo.jpg"
              />
              <HeadingText heading="Building a reputational capital">
                We’ve been working with the governmental and non-governmental
                sector for more than a decade and we are proud to say that our
                creative communications has made a difference in building the
                reputational capital and creating a social impact.
              </HeadingText>
              <div ref={fourthSection}>
                <div className="lg:max-w-7xl w-5/6 mx-auto pt-10 md:pt-16 lg:pt-24 grid sm:grid-cols-2 lg:grid-cols-3 gap-9 bg-primary">
                  <AnimatedBox
                    name="omac"
                    focus="public & ngo"
                    competence={["strategy", "content"]}
                    logo="/images/logos/OMAC.png"
                    link="creating-engaging-content-omac"
                  />
                  <AnimatedBox
                    name="eu"
                    focus="public & ngo"
                    competence={["strategy", "content", "tech", "media"]}
                    logo="/images/logos/EU.png"
                    link="public-sector-marketing-strategy-eu"
                  />
                  <AnimatedBox
                    name="nabad"
                    focus="public & ngo"
                    competence={[
                      "strategy",
                      "brand",
                      "content",
                      "tech",
                      "media",
                    ]}
                    logo="/images/logos/NABAD.png"
                    link="NGO-marketing-nabad"
                  />
                  {/* <AnimatedBox
                    name="usaid"
                    focus="public & ngo"
                    competence={["strategy", "content", "media"]}
                    logo="/images/logos/USAID.png"
                    link="usaid"
                  /> */}
                </div>
                <AnimatedLink
                  href="/work#public&ngo"
                  extraClasses="w-96 text-xl"
                  extraContainerClass="lg:max-w-7xl w-5/6 mx-auto pt-10 md:pt-16 lg:pt-24 pb-20 lg:pb-0 bg-primary relative"
                >
                  See all public & ngo work
                </AnimatedLink>
              </div>
              <HeadingImage
                ref={food}
                heading="F&B"
                image="/images/Banners/Focus/FNB.jpg"
              />
              <HeadingText heading="Transforming businesses and mindsets">
                The times have never been as interesting as they are today for
                the F&B industry. It is a total collision of tech, marketing and
                operations. We have streamlined our diversified skills at the
                service of HORECA groups for whom growth is a factor of their
                ability to reinvent themselves to become marketing led par
                excellence.
              </HeadingText>

              <div className="lg:max-w-7xl w-5/6 mx-auto pt-10 md:pt-16 lg:pt-24 grid sm:grid-cols-2 lg:grid-cols-3 gap-9 bg-primary relative">
                <AnimatedBox
                  name="yalla shawarma"
                  focus="f&b"
                  competence={["strategy", "brand", "content", "tech", "media"]}
                  logo="/images/logos/YALLA SHAWARMA.png"
                  link="cloud-kitchen-case-study-yalla-shawarma"
                />
                <AnimatedBox
                  name="diego burgers"
                  focus="f&b"
                  competence={["strategy", "brand", "content", "tech", "media"]}
                  logo="/images/logos/Diego Burgers.png"
                  link="digital-transformation-in-F&B-industry"
                />
              </div>
              <AnimatedLink
                href="/work#f&b"
                extraClasses="w-96 text-xl text-xl"
                extraContainerClass="lg:max-w-7xl w-5/6 mx-auto pt-10 md:pt-16 lg:pt-24 pb-0 lg:pb-0 bg-primary relative"
              >
                See all f&b work
              </AnimatedLink>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
