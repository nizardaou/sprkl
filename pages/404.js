export default function Custom404() {
  return <h1 className="text-7xl text-white mt-56 font-futuramaxi w-5/6 lg:max-w-7xl mx-auto h-screen">404 - Page Not Found</h1>;
}
