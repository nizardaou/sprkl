import Head from "next/head";
import AboutSection from "../components/aboutsection";
import AboutIcon from "../components/svg/abouticon";
import { useEffect, useRef } from "react";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import { gsap, Power2, Bounce } from "gsap/dist/gsap";

gsap.registerPlugin(ScrollTrigger);

const aboutItems = [
  {
    image: "/images/about/1.png",
    year: "/images/years/1.png",
    imageWidth: "404",
    imageHeight: "352",
    details: `<span class="text-cyan text-lg md:text-xl lg:text-4xl">First</Span> to localize foreign TV series into Arabic`,
    company: "Filmali International",
  },
  {
    image: "/images/about/2.png",
    year: "/images/years/2.png",
    imageWidth: "383",
    imageHeight: "386",
    details: `<span class="text-cyan text-lg md:text-xl lg:text-4xl">First</Span> to establish full service digital agency in Beirut`,
    company: "Eastline Marketing S.A.L",
  },
  {
    image: "/images/about/3.png",
    year: "/images/years/3.png",
    imageWidth: "423",
    imageHeight: "183",
    details: `<span class="text-cyan text-lg md:text-xl lg:text-4xl">First</Span> time winner of a Webby and a Gemini award. Finalist at Cannes Lions & SXSW`,
    company: "Eastline Marketing S.A.L",
  },
  {
    image: "/images/about/4.png",
    year: "/images/years/4.png",
    imageWidth: "300",
    imageHeight: "300",
    details: ` <span class="text-cyan text-lg md:text-xl lg:text-4xl"> First
    </span> to be selected as <a href="https://endeavor.org/blog/entrepreneurs/recognizing-the-power-of-digital-dreams-eastline-marketings-founders-among-executives-top-20-entrepreneurs/" target="_blank" class="underline hover:text-cyan">Endeavor high impact entrepreneurs</a> in Lebanon`,
    company: "Eastline Marketing S.A.L",
  },
  {
    image: "/images/about/5.png",
    year: "/images/years/5.png",
    imageWidth: "375",
    imageHeight: "486",
    details: `<span class="text-cyan text-lg md:text-xl lg:text-4xl">First</Span> to develop a self serve social media promotional platform in the MENA`,
    company: "Eastline Marketing S.A.L",
  },
  {
    image: "/images/about/6.png",
    year: "/images/years/6.png",
    imageWidth: "434",
    imageHeight: "279",
    details: `<span class="text-cyan text-lg md:text-xl lg:text-4xl">First</Span> to develop a full stack programmatic video ad platform in the MENA`,
    company: "SPRKL FZCO",
  },
  {
    image: "/images/about/7.png",
    year: "/images/years/7.png",
    imageWidth: "500",
    imageHeight: "162",
    details: `<span class="text-cyan text-lg md:text-xl lg:text-4xl">First</Span> movers to Amazon marketing services in the MENA region`,
    company: "SPRKL FZCO",
  },
  {
    image: "/images/about/8.png",
    year: "/images/years/8.png",
    imageWidth: "391",
    imageHeight: "392",
    details: `<span class="text-cyan text-lg md:text-xl lg:text-4xl">First</Span> steps towards global market expansion, with presence in MENA, Africa, Europe & US`,
    company: "SPRKL FZCO",
  },
];

export default function about() {
  let years = useRef([]);
  years.current = [];
  let line = useRef(null);
  let aboutIcon = useRef(null);
  let container = useRef(null);

  useEffect(() => {
    // scrollTrigger for about icon rotation animation
    ScrollTrigger.create({
      id: "about",
      start: 0,
      end: 99999,
      trigger: line.current,
      onLeaveBack: () =>
        gsap.to(aboutIcon.current, {
          rotation: "0_cw",
          delay: 0.5,
          duration: 0.3,
        }),
      onUpdate: (self) => {
        if (self.direction === 1 && self.getVelocity() / 200 > 3) {
          // console.log("first case");
          gsap.to(aboutIcon.current, {
            rotation: "0_cw",
            duration: 0.3,
          });
        } else if (self.direction === -1 && self.getVelocity() / 200 < -3) {
          // console.log("second case");
          gsap.to(aboutIcon.current, {
            rotation: "180_cw",
            duration: 0.3,
          });
        }
      },
    });

    // calculate top offset of the container
    let containerTop = container.current.getBoundingClientRect().top;

    // icon and line scroll animation based on years offset and heights
    years.current.forEach((el, index) => {
      let previousTop, oldValue, previousHeight;
      let elementInfo = el.getBoundingClientRect();
      let currentTop = elementInfo.top;
      let currentHeight = elementInfo.height;
      let newValue = currentTop - containerTop + currentHeight / 4;
      let startValue;

      if (index === 0) {
        oldValue = 0;
        startValue = 10;
      } else {
        previousTop = years.current[index - 1].getBoundingClientRect().top;
        previousHeight =
          years.current[index - 1].getBoundingClientRect().height;
        oldValue = previousTop - containerTop + previousHeight / 4;
        startValue = "top center+=250";
      }

      gsap.fromTo(
        line.current,
        {
          height: oldValue,
        },
        {
          duration: 0.5,
          height: newValue,
          ease: "none",
          scrollTrigger: {
            id: `line-${index + 1}`,
            trigger: el,
            start: startValue,
            toggleActions: "play none none reverse",
          },
        }
      );

      gsap.fromTo(
        aboutIcon.current,
        {
          top: oldValue,
        },
        {
          duration: 0.5,
          top: newValue,
          ease: "none",
          scrollTrigger: {
            id: `icon-${index + 1}`,
            trigger: el,
            start: startValue,
            toggleActions: "play none none reverse",
          },
        }
      );
    });

    // setting icon and line initial values to overide fromTo default values
    gsap.set(aboutIcon.current, { top: 0 });
    gsap.set(line.current, { height: 0 });

    // about Icon bounce animation
    gsap.to(aboutIcon.current, {
      y: -50,
      duration: 1 / 4,
      ease: Power2.easeOut,
    });
    gsap.to(aboutIcon.current, {
      y: 0,
      duration: 1 / 2,
      ease: Bounce.easeOut,
      delay: 1 / 4,
    });

    return () =>
      ScrollTrigger.getAll().forEach((t) => {
        if (t.vars.id != "menu1") t.kill();
      });
  }, []);

  const addToRefs = (el) => {
    if (el && !years.current.includes(el)) {
      years.current.push(el);
    }
  };

  return (
    <>
      <Head>
        <title> About</title>
      </Head>
      <div className="relative mx-auto w-5/6 lg:max-w-7xl bg-primary">
        <h1 className="text-4xl text-center text-white lg:text-7xl font-futuramaxi font-normal w-3/4 mx-auto mt-44 lg:mt-56 mb-24">
          <span className="text-cyan">first</span> is an instinct and a state of
          mind in action
        </h1>
        <div ref={container} className="relative py-16">
          <div>
            <div
              ref={line}
              className="absolute left-0 right-0 bg-white mx-auto w-2 top-0 h-0 rounded-lg"
            />
            <AboutIcon ref={aboutIcon} />
          </div>
          {aboutItems.map((item, index) => {
            return (
              <AboutSection
                ref={addToRefs}
                key={index}
                aboutItem={item}
                direction={index % 2}
                lastItem={index + 1 === aboutItems.length}
                firstItem={index === 0}
              />
            );
          })}
        </div>
      </div>
    </>
  );
}
